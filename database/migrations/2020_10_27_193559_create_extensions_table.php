<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extensions', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('name');
            $table->string('code')->nullable();
            $table->date('startDate');
            $table->date('endDate');
            $table->foreignId('region_id')
                ->nullable()
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
            $table->foreignId('destination_id')
                ->nullable()
                ->references('id')
                ->on('destinations')
                ->onDelete('cascade');
            $table->foreignId('category_id')
                ->nullable()
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->string('hotel')->nullable();
            $table->string('location');
            $table->string('longitude')->nullable();
            $table->string('itinerary_file')->nullable();
            $table->string('latitude')->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('facilities')->nullable();
            $table->string('rates')->nullable();
            $table->string('coin')->nullable();
            $table->integer('simple_cash')->nullable();
            $table->integer('double_cash')->nullable();
            $table->integer('triple_cash')->nullable();
            $table->integer('simple_card')->nullable();
            $table->integer('double_card')->nullable();
            $table->integer('triple_card')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('pack_city')->nullable();
            $table->boolean('museum_package')->nullable();
            $table->string('a_circuit')->nullable();
            $table->string('circuit_days')->nullable();
            $table->string('food_plan')->nullable();
            $table->string('language_plan')->nullable();
            $table->string('cruise_from_to')->nullable();
            $table->string('cruise_days')->nullable();
            $table->string('go_suitable_transfer')->nullable();
            $table->string('go_transfer_from_to')->nullable();
            $table->string('train_tickets')->nullable();
            $table->string('train_tickets_from_to')->nullable();
            $table->string('ferry')->nullable();
            $table->string('car_rental')->nullable();
            $table->string('rent_car_days')->nullable();
            $table->string('walk')->nullable();
            $table->string('foods')->nullable();
            $table->string('dinners')->nullable();
            $table->string('excursion_1')->nullable();
            $table->string('excursion_2')->nullable();
            $table->string('excursion_3')->nullable();
            $table->string('entry')->nullable();
            $table->string('passes')->nullable();
            $table->string('see_another_1')->nullable();
            $table->string('see_another_2')->nullable();
            $table->string('see_another_3')->nullable();
            $table->string('guide')->nullable();
            $table->string('local_guide')->nullable();
            $table->string('sure')->nullable();
            $table->string('health_insurance')->nullable();
            $table->string('visa_for')->nullable();
            $table->boolean('assistance')->nullable();
            $table->boolean('trunks')->nullable();
            $table->boolean('charges')->nullable();
            $table->boolean('taxes')->nullable();
            $table->boolean('emergency')->nullable();
            $table->boolean('local_contact')->nullable();
            $table->boolean('folders')->nullable();
            $table->boolean('labels')->nullable();
            $table->boolean('itinerary')->nullable();
            $table->boolean('destination')->nullable();
            $table->boolean('tips')->nullable();
            $table->boolean('exhibitor_tips')->nullable();
            $table->string('type')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extensions');
    }
}
