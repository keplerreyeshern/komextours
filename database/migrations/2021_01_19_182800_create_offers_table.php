<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('name');
            $table->string('code')->nullable();
            $table->date('startDate');
            $table->date('endDate');
            $table->foreignId('region_id')
                ->nullable()
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
            $table->foreignId('destination_id')
                ->nullable()
                ->references('id')
                ->on('destinations')
                ->onDelete('cascade');
            $table->foreignId('category_id')
                ->nullable()
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->foreignId('fair_id')
                ->nullable()
                ->references('id')
                ->on('fairs')
                ->onDelete('cascade');
            $table->string('hotel')->nullable();
            $table->string('location')->nullable();;
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('facilities')->nullable();
            $table->string('rates')->nullable();
            $table->string('coin')->nullable();
            $table->integer('simple_cash')->nullable();
            $table->integer('double_cash')->nullable();
            $table->integer('triple_cash')->nullable();
            $table->integer('simple_card')->nullable();
            $table->integer('double_card')->nullable();
            $table->integer('triple_card')->nullable();
            $table->boolean('active')->default(true);
            $table->string('guide')->nullable();
            $table->string('local_guide')->nullable();
            $table->string('sure')->nullable();
            $table->string('health_insurance')->nullable();
            $table->string('visa_for')->nullable();
            $table->boolean('assistance')->nullable();
            $table->boolean('trunks')->nullable();
            $table->boolean('charges')->nullable();
            $table->boolean('taxes')->nullable();
            $table->boolean('emergency')->nullable();
            $table->boolean('local_contact')->nullable();
            $table->boolean('folders')->nullable();
            $table->boolean('labels')->nullable();
            $table->boolean('itinerary')->nullable();
            $table->boolean('destination')->nullable();
            $table->boolean('tips')->nullable();
            $table->boolean('exhibitor_tips')->nullable();
            $table->string('type')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
