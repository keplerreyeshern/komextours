<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtensionOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extension_offer', function (Blueprint $table) {
            $table->id();
            $table->foreignId('extension_id')
                ->references('id')
                ->on('extensions')
                ->onDelete('cascade');
            $table->foreignId('offer_id')
                ->references('id')
                ->on('offers')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extension_offer');
    }
}
