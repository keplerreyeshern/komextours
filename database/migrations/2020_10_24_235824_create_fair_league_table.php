<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFairLeagueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fair_league', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fair_id')
                ->references('id')
                ->on('fairs')
                ->onDelete('cascade');
            $table->foreignId('league_id')
                ->references('id')
                ->on('leagues')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fairs_leagues');
    }
}
