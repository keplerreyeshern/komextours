<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fairs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('url')->nullable();
            $table->date('startDate');
            $table->date('endDate');
            $table->foreignId('sector_id')
                ->references('id')
                ->on('sectors')
                ->onDelete('cascade');
            $table->string('place')->nullable();
            $table->string('short_description')->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('services', 1000)->nullable();
            $table->string('video')->nullable();
            $table->string('poster')->nullable();
            $table->string('logo')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('location')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('star')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fairs');
    }
}
