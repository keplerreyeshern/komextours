<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Offer extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "offers";

    protected $translatable = [
        'slug', 'name', 'code', 'description', 'facilities', 'rates', 'coin','guide', 'local_guide',
        'sure', 'health_insurance', 'visa_for'
    ];

    protected $fillable = [
        'slug', 'name', 'code', 'startDate', 'endDate', 'region_id', 'destination_id', 'category_id', 'fair_id',
        'hotel', 'location', 'longitude', 'latitude', 'description', 'facilities', 'rates', 'coin',
        'simple_cash', 'double_cash', 'triple_cash', 'simple_card', 'double_card', 'triple_card',
        'active', 'guide', 'local_guide', 'sure', 'health_insurance', 'visa_for', 'assistance',
        'trunks', 'charges', 'taxes', 'emergency', 'local_contact', 'folders', 'labels', 'itinerary',
        'destination', 'tips', 'exhibitor_tips', 'type'
    ];

    public function fair(){
        return $this->belongsTo('App\Models\Fair');
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

    public function destino(){
        return $this->belongsTo(Destination::class, 'destination_id');
    }

    public function leagues(){
        return $this->belongsToMany('App\Models\League')->withTimestamps();
    }
}
