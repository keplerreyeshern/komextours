<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class News extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "news";
    public $translatable = ['slug', 'title', 'intro', 'content'];

    public $fillable = ['title', 'intro', 'content', 'image', 'active', 'date'];
}
