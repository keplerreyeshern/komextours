<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Fair extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "fairs";

    protected $translatable = ['name', 'slug', 'place', 'short_description', 'description', 'services'];

    protected $fillable = [
        'name',
        'url',
        'active',
        'startDate',
        'endDate',
        'sector_id',
        'video',
        'poster',
        'logo',
        'latitude',
        'longitude',
        'place',
        'short_description',
        'description',
        'services',
        'location',
        'start'
    ];

    public function leagues(){
        return $this->belongsToMany('App\Models\League')->withTimestamps();
    }

    public function sector(){
        return $this->belongsTo('App\Models\Sector');
    }

    public function offer(){
        return $this->hasOne('App\Models\Extension');
    }
}
