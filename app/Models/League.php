<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class League extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "leagues";
    public $translatable = ['name'];

    public $fillable = ['name', 'url', 'active'];
}
