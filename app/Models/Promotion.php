<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Promotion extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "promotions";
    public $translatable = ['name', 'content'];

    public $fillable = ['name', 'url', 'content', 'image', 'active'];
}
