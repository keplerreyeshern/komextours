<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Extension extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "extensions";

    protected $translatable = [
        'slug', 'name', 'code', 'description', 'facilities', 'rates', 'coin','a_circuit', 'circuit_days',
        'food_plan', 'language_plan', 'cruise_from_to', 'cruise_days', 'go_suitable_transfer',
        'go_transfer_from_to', 'train_tickets', 'train_tickets_from_to', 'ferry', 'car_rental',
        'rent_car_days', 'walk', 'foods', 'dinners', 'excursion_1', 'excursion_2', 'excursion_3',
        'entry', 'passes', 'see_another_1', 'see_another_2', 'see_another_3', 'guide', 'local_guide',
        'sure', 'health_insurance', 'visa_for'
    ];

    protected $fillable = [
        'slug', 'name', 'code', 'startDate', 'endDate', 'region_id', 'destination_id', 'category_id',
        'hotel', 'location', 'longitude', 'latitude', 'description', 'facilities', 'rates', 'coin',
        'simple_cash', 'double_cash', 'triple_cash', 'simple_card', 'double_card', 'triple_card',
        'active', 'city_pack', 'museum_package', 'a_circuit', 'circuit_days', 'food_plan', 'language_plan',
        'cruise_from_to', 'cruise_days', 'go_suitable_transfer', 'go_transfer_from_to', 'train_tickets',
        'train_tickets_from_to', 'ferry', 'car_rental', 'rent_car_days', 'walk', 'foods', 'dinners',
        'excursion_1', 'excursion_2', 'excursion_3', 'entry', 'passes', 'see_another_1', 'see_another_2',
        'see_another_3', 'guide', 'local_guide', 'sure', 'health_insurance', 'visa_for', 'assistance',
        'trunks', 'charges', 'taxes', 'emergency', 'local_contact', 'folders', 'labels', 'itinerary',
        'destination', 'tips', 'exhibitor_tips', 'type', 'itinerary_file',
    ];

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

    public function destino(){
        return $this->belongsTo(Destination::class, 'destination_id');
    }

    public function destinoMexico(){
        return $this->belongsTo(Region::class, 'destination_id');
    }

    public function leagues(){
        return $this->belongsToMany('App\Models\League')->withTimestamps();
    }
}
