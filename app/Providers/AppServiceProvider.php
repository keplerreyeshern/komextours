<?php

namespace App\Providers;

use App\Models\DestinationManagement;
use App\Models\Fair;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale(config('app.locale'));
        Paginator::defaultView('vendor.pagination.bootstrap-4');
        Paginator::defaultSimpleView('vendor.pagination.simple-bootstrap-4');
        view()->composer('public/layouts/siderbar', function($view) {
            $collection = Fair::where('star', true)->get();
            if(!$collection->isEmpty()){
                $fair = $collection->random(1);
            } else {
                $fair = [];
            }
            $view->with('fair', $fair);
        });

        view()->composer('public/layouts/siderbar', function($view) {
            $managements = DestinationManagement::where('active', true)->get();
            $view->with('managements', $managements);
        });
    }
}
