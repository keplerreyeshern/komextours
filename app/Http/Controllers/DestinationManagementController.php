<?php

namespace App\Http\Controllers;

use App\Models\DestinationManagement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class DestinationManagementController extends Controller
{

    protected $title;
    protected $active;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Destination Management";

        View::share ('title', $this->title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managements = DestinationManagement::get();
        return view('administrador.managements.list', compact('managements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.managements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'url' => 'required',
        ]);
        $management = new DestinationManagement();
        $management->url = $data['url'];
        $management->active = true;
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/management/' . $nameImage, \File::get($image));
            $management->image = '/storage/images/management/'.$nameImage;
        }
        $management->save();
        return redirect(route('management.index'))->with('success', '¡Destination Management se creo con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DestinationManagement  $destinationManagement
     * @return \Illuminate\Http\Response
     */
    public function show(DestinationManagement $destinationManagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DestinationManagement  $destinationManagement
     * @return \Illuminate\Http\Response
     */
    public function edit(DestinationManagement $management)
    {
        return view('administrador.managements.edit', compact('management'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DestinationManagement  $destinationManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DestinationManagement $management)
    {
        $data = $request->validate([
            'url' => 'required',
        ]);
        $management->url = $data['url'];
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/management/' . $nameImage, \File::get($image));
            $management->image = '/storage/images/management/'.$nameImage;
        }
        $management->save();
        return redirect(route('management.index'))->with('success', '¡Destination Management se actualizo con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DestinationManagement  $destinationManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestinationManagement $management)
    {
        $management->delete();
        return redirect(route('management.index'))->with('success', '¡Destination Management se elimino con exito!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $management = DestinationManagement::findOrFail($id);
        if($management->active){
            $management->active = false;
            $result = 'desactivo';
        } else {
            $management->active = true;
            $result = 'activo';
        }
        $management->save();
        return redirect(route('management.index'))->with('success', 'Destination Management se '. $result.' correctamente');
    }
}
