<?php

namespace App\Http\Controllers;

use App\Models\Extension;
use App\Models\Fair;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Quotation;
use Illuminate\Support\Facades\Redirect;

class EmailsController extends Controller
{
    function quotation(Request $request){
        if ($request['type'] != 'fairs'){
            $extension = Extension::where('id', $request['reference'])->first();
            $name = $extension->name;
        } else {
            $extension = Fair::where('id', $request['reference'])->first();
            $name = $extension->name;
        }

        $data = [
            'name' => $request['name'],
            'email' => $request['email'],
            'telephone' => $request['telephone'],
            'date' => $request['date'],
            'company' => $request['company'],
            'location' => $request['location'],
            'visit' => $request['visit'],
            'days' => $request['days'],
            'duration' => $request['duration'],
            'no_persons' => $request['no_persons'],
            'notes' => $request['notes'],
            'type' => $request['type'],
            'nameExtension' => $name
        ];

        Mail::to('trujillo@excess.com.mx', 'Representante de Ventas')
            ->send(new Quotation($data));

        return Redirect::back();
    }
}
