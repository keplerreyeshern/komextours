<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class NewsController extends Controller
{

    protected $title;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = 'Noticias';
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsAll = News::where('type', 'news')->orderBy('date', 'asc')->get();
        return view('administrador.news.list', compact('newsAll'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $data = $request->validate([
            'title' => 'required',
            'intro' => 'required',
            'content' => 'required'
        ],[
            'title.required' => 'El campo titulo es obligatorio',
            'intro.required' => 'El campo introducción es obligatorio',
            'content.required' => 'El campo contenido es obligatorio',
        ]);
        $news = new News();
        $news->title = $data['title'];
        $news->slug = Str::slug($data['title']);
        $news->intro = $data['intro'];
        $news->content = $data['content'];
        $news->date = date_create('now');
        $news->type = 'news';
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/news/' . $nameImage, \File::get($image));
            $news->image = '/storage/images/news/'.$nameImage;
        }
        $news->save();

        return redirect('admin/news')->with('success', 'Se creo con exito la noticia');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('administrador.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $image = $request->file('image');
        $data = $request->validate([
            'title' => 'required',
            'intro' => 'required',
            'content' => 'required'
        ],[
            'title.required' => 'El campo titulo es obligatorio',
            'intro.required' => 'El campo introducción es obligatorio',
            'content.required' => 'El campo contenido es obligatorio',
        ]);
        $news->title = $data['title'];
        $news->intro = $data['intro'];
        $news->content = $data['content'];
        $news->date = date_create('now');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/news/' . $nameImage, \File::get($image));
            $news->image = '/storage/images/news/'.$nameImage;
        }
        $news->save();
        return redirect('admin/news')->with('success', 'Se actualizo con exito la noticia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect('admin/news')->with('success', 'Se elimino con exito la noticia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $news = News::findOrFail($id);
        if($news->active){
            $news->active = false;
            $result = 'desactivo';
        } else {
            $news->active = true;
            $result = 'activo';
        }
        $news->save();
        return redirect('admin/news')->with('success', 'La noticia se '. $result.' correctamente');
    }
}
