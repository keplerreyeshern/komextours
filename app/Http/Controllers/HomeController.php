<?php

namespace App\Http\Controllers;

use App\Models\Fair;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    protected $title;
    protected $active;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->title = "Mi Cuenta";


        View::share ('title', $this->title);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fairs = Fair::whereDate('endDate', '<', Carbon::now()->format('Y-m-d'))->get();
        foreach ($fairs as $fair){
            $fair->active = false;
            $fair->save();
        }
        return view('administrador.home');
    }
}
