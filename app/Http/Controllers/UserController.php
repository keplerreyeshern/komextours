<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Destination;
use App\Models\League;
use App\Models\Region;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{

    protected $title;
    protected $type;
    protected $url;
    protected $single;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Usuarios";
        $this->type = "users";
        $this->single = "Usuario";
        $this->url = 'usuarios';

        $destinations = Destination::get();
        $regions = Region::get();
        $categories = Category::get();
        $leagues = League::get();

        View::share('type', $this->type);
        View::share('url', $this->url);
        View::share('title', $this->title);
        View::share('single', $this->single);
        View::share('destinations', $destinations);
        View::share('regions', $regions);
        View::share('categories', $categories);
        View::share('leagues', $leagues);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('administrador.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' =>'required|string|max:255',
            'email' =>'required|string|email|max:255|unique:users',
            'password' =>'required|string|min:8|confirmed',
        ], [
            'name.required' =>'El campo nombre es obligatorio',
            'name.string' =>'El campo nombre debe ser texto',
            'name.max' =>'El campo nombre debe tener como maximo 255 caracteres',
            'email.required' =>'El campo email es obligatorio',
            'email.max' =>'El campo email debe tener como maximo 255 caracteres',
            'email.unique' =>'El campo email ya se encuentra en la base de datos',
            'email.email' =>'el campo email debe tener un formato valido',
            'password.required' =>'El campo contraseña es obligatorio',
            'password.string' =>'El campo contraseña debe ser texto',
            'password.min' =>'El campo contraseña debe tener como minimo 8 caracteres',
            'password.confirmed' =>'Las contraseñas deben coincidir',
        ]);
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect('/admin/users')->with('success', 'El usuario se creo correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('administrador.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->validate([
            'name' =>'required|string|max:255',
            'email' =>'required|string|email|max:255|unique:users,id',
            'password' =>'required|string|min:8|confirmed',
        ], [
            'name.required' =>'El campo nombre es obligatorio',
            'name.string' =>'El campo nombre debe ser texto',
            'name.max' =>'El campo nombre debe tener como maximo 255 caracteres',
            'email.required' =>'El campo email es obligatorio',
            'email.max' =>'El campo email debe tener como maximo 255 caracteres',
            'email.unique' =>'El campo email ya se encuentra en la base de datos',
            'email.email' =>'el campo email debe tener un formato valido',
            'password.required' =>'El campo contraseña es obligatorio',
            'password.string' =>'El campo contraseña debe ser texto',
            'password.min' =>'El campo contraseña debe tener como minimo 8 caracteres',
            'password.confirmed' =>'Las contraseñas deben coincidir',
        ]);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();

        return redirect('/admin/users')->with('success', 'El usuario se actualizo correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('admin/users')->with('success', 'El usuario se elimino con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $user = User::findOrFail($id);
        if($user->active){
            $user->active = false;
            $result = 'desactivo';
        } else {
            $user->active = true;
            $result = 'activo';
        }
        $user->save();
        return redirect('admin/users')->with('success', 'La usuario se '. $result.' correctamente');
    }
}
