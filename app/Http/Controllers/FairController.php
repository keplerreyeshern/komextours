<?php

namespace App\Http\Controllers;

use App\Models\Fair;
use App\Models\Image;
use App\Models\League;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class FairController extends Controller
{
    protected $title;
    protected $active;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Ferias";

        $sectors = Sector::get();
        $leagues = League::get();

        View::share ('title', $this->title);
        View::share ('sectors', $sectors);
        View::share ('leagues', $leagues);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fairs = Fair::get();
        return view('administrador.fairs.list', compact('fairs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.fairs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request['active'])){
            $active = true;
        } else {
            $active =false;
        }
        $data = $request->validate([
            'name' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'sector' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'place' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'services' => 'required',
            'location' => 'required',
        ],[]);
        $fair = new Fair();
        $fair->name = $data['name'];
        $fair->slug = Str::slug($data['name']);
        $fair->url = $request['url'];
        $fair->active = $active;
        $fair->startDate = $data['startDate'];
        $fair->endDate = $data['endDate'];
        $fair->sector_id = $data['sector'];
        $fair->video = $request['video'];
        $fair->latitude = $request['latitude'];
        $fair->longitude = $request['longitude'];
        $fair->place = $data['place'];
        $fair->short_description = $data['short_description'];
        $fair->description = $data['description'];
        $fair->services = $data['services'];
        $fair->location = $data['location'];
        $poster = $request->file('poster');
        if($poster) {
            $namePoster = time() . "_" . $poster->getClientOriginalName();
            Storage::disk('public')->put('/images/fairs/' . $namePoster, \File::get($poster));
            $fair->poster = '/storage/images/fairs/'.$namePoster;
        }
        $logo = $request->file('logo');
        if($logo) {
            $nameLogo = time() . "_" . $logo->getClientOriginalName();
            Storage::disk('public')->put('/images/fairs/' . $nameLogo, \File::get($logo));
            $fair->logo = '/storage/images/fairs/'.$nameLogo;
        }
        $fair->save();
        $fair->leagues()->sync($request['leagues']);
        $images = Image::whereNull('reference')->where('type', 'fairs')->get();
        if($images){
            for($i=0;count($images)>$i;$i++){
                $images[$i]->reference = $fair->id;
                $images[$i]->save();
            }
        }
        return redirect('admin/fairs')->with('success', '¡La feria se creo con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $fair = Fair::where('slug', 'LIKE', '%'.$slug.'%' )->first();
        $images = Image::where('type', 'extensions')->where('reference', $fair->id)->get();

        return view('public.main.event', compact('fair', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fair = Fair::findOrFail($id);
        $images = Image::where('type', 'fairs')->where('reference', $id)->get();
        return view('administrador.fairs.edit', compact('fair', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request['active'])){
            $active = true;
        } else {
            $active =false;
        }
        $data = $request->validate([
            'name' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'sector' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'place' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'services' => 'required',
            'location' => 'required',
        ],[]);
        $fair = Fair::findOrFail($id);
        $fair->name = $data['name'];
        $fair->url = $request['url'];
        $fair->active = $active;
        $fair->startDate = $data['startDate'];
        $fair->endDate = $data['endDate'];
        $fair->sector_id = $data['sector'];
        $fair->video = $request['video'];
        $fair->latitude = $request['latitude'];
        $fair->longitude = $request['longitude'];
        $fair->place = $data['place'];
        $fair->short_description = $data['short_description'];
        $fair->description = $data['description'];
        $fair->services = $data['services'];
        $fair->location = $data['location'];
        $poster = $request->file('poster');
        if($poster) {
            $namePoster = time() . "_" . $poster->getClientOriginalName();
            Storage::disk('public')->put('/images/fairs/' . $namePoster, \File::get($poster));
            $fair->poster = '/storage/images/fairs/'.$namePoster;
        }
        $logo = $request->file('logo');
        if($logo) {
            $nameLogo = time() . "_" . $logo->getClientOriginalName();
            Storage::disk('public')->put('/images/fairs/' . $nameLogo, \File::get($logo));
            $fair->logo = '/storage/images/fairs/'.$nameLogo;
        }
        $fair->save();
        $fair->leagues()->sync($request['leagues']);
        return redirect('admin/fairs')->with('success', '¡La feria se actualizo con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fair = Fair::findOrFail($id);
        $fair->delete();
        return redirect('admin/fairs')->with('success', '¡La feria se elimino con exito!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $fair = Fair::findOrFail($id);
        if($fair->active){
            $fair->active = false;
            $result = 'desactivo';
        } else {
            $fair->active = true;
            $result = 'activo';
        }
        $fair->save();
        return redirect('admin/fairs')->with('success', 'La feria se '. $result.' correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function star($id)
    {
        $fair = Fair::findOrFail($id);
        if($fair->star){
            $fair->star = false;
            $result = 'borro de ';
        } else {
            $fair->star = true;
            $result = 'agrego a ';
        }
        $fair->save();
        return redirect('admin/fairs')->with('success', 'La feria se '. $result.' la lista de destacados');
    }
}
