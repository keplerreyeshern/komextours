<?php

namespace App\Http\Controllers;

use App\Models\League;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class LeagueController extends Controller
{
    protected $title;
    protected $active;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->title = "Ligas";

        View::share ('title', $this->title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leagues = League::get();

        return view('administrador.leagues.list', compact('leagues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.leagues.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'league' => 'required'
        ],[
            'name.required' => 'EL campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'league.required' => 'El campo liga es obligatorio'
        ]);

        League::create([
            'name' => $data['name'],
            'url' => $data['league']
        ]);
        return redirect('/admin/leagues')->with('success', '¡La liga se creo con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $league = League::find($id);
        return view('administrador.leagues.edit', compact('league'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $league = League::find($id);
        $data = $request->validate([
            'name' => 'required|string',
            'league' => 'required'
        ],[
            'name.required' => 'EL campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'league.required' => 'El campo liga es obligatorio'
        ]);
        $league->name = $data['name'];
        $league->url = $data['league'];
        $league->save();
        return redirect('/admin/leagues')->with('success', '¡La liga se actualizo con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $league = League::find($id);
        $league->delete();
        return redirect('/administrador/leagues')->with('success', '¡La liga se elimino con éxito!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $league = League::find($id);
        if ($league->active){
            $league->active = false;
            $result = 'desactivo';
        } else {
            $league->active = true;
            $result = 'activo';
        }
        $league->save();
        return redirect('/admin/leagues')->with('success', '¡La liga se ' .$result. ' con éxito!');
    }
}
