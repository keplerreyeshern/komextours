<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('storage-link', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
});

Auth::routes();

Route::post('quotation', [\App\Http\Controllers\EmailsController::class, 'quotation'])->name('quotation');


Route::group(['middleware' => ['lang']], function () {
    Route::get('lang/{lang}', function ($lang) {
        session(['lang' => $lang]);
        return \Redirect::back();
    })->where([
        'lang' => 'en|es'
    ]);
    Route::get('/', [\App\Http\Controllers\MainController::class, 'index'])->name('index');
    Route::get('/noticias', [\App\Http\Controllers\MainController::class, 'index'])->name('news');
    Route::get('/testimoniales', [\App\Http\Controllers\MainController::class, 'index'])->name('testimonials');
    Route::get('/aviso-de-privacidad', [\App\Http\Controllers\MainController::class, 'noticePrivacy'])->name('noticeOfPrivacy');
    Route::get('/quienes-somos', [\App\Http\Controllers\MainController::class, 'about'])->name('about');
    Route::get('/servicios/{item}', [\App\Http\Controllers\MainController::class, 'services'])->name('services');
    Route::get('/eventos', [\App\Http\Controllers\MainController::class, 'events'])->name('events');
    Route::get('/eventos/{slug}', [\App\Http\Controllers\FairController::class, 'show'])->name('event');
    Route::post('/events/component', [\App\Http\Controllers\MainController::class, 'eventsChange']);
    Route::get('/extensiones', [\App\Http\Controllers\MainController::class, 'extensions'])->name('extensions');
    Route::get('/extensiones/{slug}', [\App\Http\Controllers\ExtensionController::class, 'show'])->name('extension');
    Route::post('/extensions/component', [\App\Http\Controllers\MainController::class, 'extensionsChange']);
    Route::get('/mexico', [\App\Http\Controllers\MainController::class, 'mexico'])->name('mexico');
    Route::get('/mexico/{slug}', [\App\Http\Controllers\MexicoController::class, 'show'])->name('mex');
    Route::post('/mexico/component', [\App\Http\Controllers\MainController::class, 'mexicoChange']);
    Route::get('/viajes-especiales', [\App\Http\Controllers\MainController::class, 'specialTrips'])->name('specialTrips');
    Route::get('/viajes-especiales/{slug}', [\App\Http\Controllers\SpecialTripsContoller::class, 'show'])->name('specialTrip');
    Route::post('/viajes_especiales/component', [\App\Http\Controllers\MainController::class, 'specialTripsChange']);
    Route::get('/testimoniales', [\App\Http\Controllers\MainController::class, 'testimonials'])->name('testimonials');
    Route::get('/noticias', [\App\Http\Controllers\MainController::class, 'news'])->name('news');
    Route::get('/noticias/{slug}', [\App\Http\Controllers\MainController::class, 'newsShow'])->name('newsShow');
    Route::get('/blog', [\App\Http\Controllers\MainController::class, 'blogs'])->name('blogs');
    Route::get('/blog/{slug}', [\App\Http\Controllers\MainController::class, 'blog'])->name('blog');
    Route::get('/ofertas', [\App\Http\Controllers\MainController::class, 'offers'])->name('offers');
    Route::post('/offers/component', [\App\Http\Controllers\MainController::class, 'offersChange']);
    Route::get('/ofertas/{slug}', [\App\Http\Controllers\OfferController::class, 'show'])->name('offer');
    Route::get('/contacto', [\App\Http\Controllers\MainController::class, 'contact'])->name('contact');
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'lang']], function (){
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('/leagues', \App\Http\Controllers\LeagueController::class)->except(['show']);
    Route::post('/leagues/active/{id}', [\App\Http\Controllers\LeagueController::class, 'active'])->name('leagues.active');
    Route::resource('/fairs', \App\Http\Controllers\FairController::class)->except(['show']);
    Route::post('/fairs/active/{id}', [\App\Http\Controllers\FairController::class, 'active'])->name('fairs.active');
    Route::post('/fairs/star/{id}', [\App\Http\Controllers\FairController::class, 'star'])->name('fairs.star');
    Route::resource('/extensions', \App\Http\Controllers\ExtensionController::class)->except(['show']);
    Route::post('/extensions/active/{id}', [\App\Http\Controllers\ExtensionController::class, 'active'])->name('extensions.active');
    Route::resource('/mexico', \App\Http\Controllers\MexicoController::class)->except(['show']);
    Route::post('/mexico/active/{id}', [\App\Http\Controllers\MexicoController::class, 'active'])->name('mexico.active');
    Route::resource('/trips', \App\Http\Controllers\SpecialTripsContoller::class)->except(['show']);
    Route::post('/trips/active/{id}', [\App\Http\Controllers\SpecialTripsContoller::class, 'active'])->name('trips.active');
    Route::resource('/news', \App\Http\Controllers\NewsController::class)->except(['show']);
    Route::post('/news/active/{id}', [\App\Http\Controllers\NewsController::class, 'active'])->name('news.active');
    Route::resource('/blogs', \App\Http\Controllers\BlogController::class)->except(['show']);
    Route::post('/blogs/active/{id}', [\App\Http\Controllers\BlogController::class, 'active'])->name('blogs.active');
    Route::resource('/offers', \App\Http\Controllers\OfferController::class)->except(['show']);
    Route::post('/offers/active/{id}', [\App\Http\Controllers\OfferController::class, 'active'])->name('offers.active');
    Route::resource('/images', \App\Http\Controllers\ImageController::class)->except(['show', 'index', 'update', 'edit']);
    Route::get('/images/{id}', [\App\Http\Controllers\ImageController::class, 'destroy'])->name('images.destroy');
    Route::resource('/promotions', \App\Http\Controllers\PromotionController::class)->except(['show']);
    Route::post('/promotions/active/{id}', [\App\Http\Controllers\PromotionController::class, 'active'])->name('promotions.active');
    Route::resource('/users', \App\Http\Controllers\UserController::class)->except(['show']);
    Route::post('/users/active/{id}', [\App\Http\Controllers\UserController::class, 'active'])->name('users.active');
    Route::resource('/destination/management', \App\Http\Controllers\DestinationManagementController::class)->except(['show']);
    Route::post('/destination/management/active/{id}', [\App\Http\Controllers\DestinationManagementController::class, 'active'])->name('management.active');
});
