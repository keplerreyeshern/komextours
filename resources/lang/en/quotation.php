<?php

return [
    'request' => 'Request Quote',
    'industry' => 'Industry',
    'date' => 'Date',
    'description' => 'Description',
    'services' => 'Services',
    'location' => 'Location',
    'notes' => 'Notes',
    'persons' => 'No de Persons',
    'duration' => 'Duration',
    'days' => 'Days',
    'visit' => 'Visitor / Exhibitor',
    'company' => 'Company',
    'telephone' => 'Telephone',
    'email' => 'Email',
    'name' => 'Full Name',
];
