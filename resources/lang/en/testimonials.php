<?php

return [
    'component' => [
        'all' => 'See All',
        'title1' => 'Luna, Mexico - Special trip to India & Nepal (2012)',
        'content1' => '"The trip led me to confront myself with a fantastic explosion of colors, delicious and new flavors and smells, all taken to their maximum expression. Observing, participating and respecting other religions and different ways of thinking that see life in a way so peculiar and different from mine. Visiting the most exquisite, imposing, majestic and splendid palaces, temples and forts along the trip, evoked very different and pleasurable feelings and emotions.
                        The flight over the Himalayas left me breathless, speechless, taking me to the top of the world and of my most cherished dreams. The common people, always smiling and kind, made me think that a world of Peace is possible.
                        Our wonderful travel companions, always attentive, cordial and willing to enjoy the day to day, made the trip a success. Suzanne and Betty, who not only organized the trip beautifully with the highest quality, surprises and many details, but also enjoyed and enjoyed every moment with us, to both of them, I thank you, for having made this trip to India simply"',
        'title2' => 'Mr. L. Castro, Waltvick from Mexico Travel Fair with extension (November 2013)',
        'content2' => '"Everything was excellent, in general both hotels and transfers were quite good! We will keep in touch for future events."',
        'title3' => 'E. Romero, Impulsora Euro, Mexico - Travel Fair with extension (January 2014)',
        'content3' => '"It went very well. We loved the recommendation of Kitzbuhel and Innsbruck. The hotels very good."',
        'title4' => 'P. Gonzalez, Optical Growging, Mexico - Trade Fair trip with extension (February 2014)',
        'content4' => '"It went very well, everything was excellent. The hotels were mega clean and with everything we needed, the tours were very cool, we were very happy with the service provided."',
        'title5' => 'G. Gonzalez, DE International de Mexico - Single Trip (February 2014)',
        'content5' => '"The tour you organized for us with the local travel agency, was excellent, as well as the guide Oleg´s knowledge of history, art and Spanish language. Thank you very much again and we reiterate that it was a very well put together trip thanks to the tours and recommendations by KOMEX Royale tours."',
        'title6' => 'Mohamed Amr, Executive Manager, Decastro Tours, Egypt - Honeymoon in Mexico (October 2013)',
        'content6' => '"Greetings from Decastro Tours Egypt. Last time I booked through you was for my honeymoon which was great and had a lovely choice in Cancun and everything was perfect from you and your staff. Thank you,"',
        'title7' => 'Sophie Wright, Operations Supervisor, Martin Randall Travel Ltd, UK - Mexico Visit (November 2013)',
        'content7' => '"Thank you very much for arranging my trip to Mexico - I had a great time and have now returned to the office to work on an itinerary."',
        'title8' => 'Jimmy Liu, CBT Holidays, Australia - Mexico Tour Group (December 2013)',
        'content8' => '"The trip was great and guides are good! Just return back Sydney and looking the group going in April."',
        'title9' => 'Božena Teissingová, White Grant s.r.o., Czech Rep. - Group on tour in Mexico (April 2014)',
        'content9' => '"I would like to thank you very much for everything you did for my group, especially for Lula. I have not found many guides like her. She is excellent and gave us a lot of pleasure to meet her. All the clients were very happy, they liked Mexico very much and you know how it is. If the clients are happy, it is the best prize we can get. I hope there will be more tourists to Mexico in the future even though we are a small agency with permanent clients who are already making plans for another trip to another country. Thank you very much again and I send cordial greetings from Prague to everyone in the office."',
        'title10' => 'Mrs. N. Aizpuru - Special trip to Portugal and Morocco (April 2015)',
        'content10' => '"The truth is that I was delighted with this trip and with you! Whoever I tell, I cant stop cheering for you and it´s the truth.  I had sworn never to go on a tour to Europe again after a bitter experience 30 something years ago but I was fascinated. Excellent tours, excellent organization, excellent hotels and restaurants but most of all excellent people and a great group"',
        'title11' => 'Mrs. M. Sanchez- Trip to Portugal and Morocco (April 2015)',
        'content11' => '"Thank you my dear Bety and Suzanne. With your professionalism, affection and great passion for your work, you have been the backbone and soul of this endearing travel group that has enriched us so much by making these wonderful trips more enjoyable."',
    ],
];
