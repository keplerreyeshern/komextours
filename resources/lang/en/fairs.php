<?php

return [

    'title' => 'Fairs',
    'description' => 'Did you know that traveling to an International Fair involves up to 20 or more different negotiations with suppliers and that we simplify the whole process, with personalized advice that we will provide you?',
    'collapse' => [
        'one' => 'Proven experience of more than 14 years',
        'two' => 'We basically know the fairs, the place, the hotels; we have a complete know-how of everything that a fair implies',
        'three' => 'We ensure your place at the fair and in nearby hotels',
        'four' => 'We have alliances with the organizers of the main International Fairs',
        'five' => 'We give you a comprehensive solution to your business trip and we can organize your Tourist Extension from the place of the fair',
    ],
    'search' => 'Search by Name',
    'select' => 'Select an Industry',
    'date' => 'Date',
    'place' => 'Place',
    'sector' => 'Industry',
    'more' => 'More Information',
    'empty' => 'They did not find fairs with those parameters',
    'show' => [
        'sector' => 'industry',
        'date' => 'Date',
        'facilities' => 'Facilities:',
        'rates' => 'Rates:',
        'services' => 'Services',
        'description' => 'Description',
        'inclusions' => 'Inclusions',
        'place' => 'Location',
        'leagues' => 'Leagues',
        'offers' => [
            'title' => 'Offers',
            'empty' => 'Soon here you will find the most interesting offers related to this fair',
        ],
    ],

];
