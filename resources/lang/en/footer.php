<?php

return [
    'street' => 'Street:',
    'suburb' => 'Suburb.',
    'side' => 'Free side:',
    'telephone' => 'Tel (switch)',
    'emergencies' => 'Emergencies',
    'follow' => 'Follow us on:',
    'copyright' => 'All rights reserved.',
    'privacy' => 'Privacy Notice',
    'update' => 'Last Update',
];
