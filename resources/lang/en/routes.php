<?php

return [
    'news' => [
        'principal' => '/news',
    ],
    'testimonials' => '/testimonials',
];
