<?php

return [


    'title' => 'About Us',
    'history' => [
        'title' => 'History',
        'paragraph1' => 'KOMEX tours, SA de CV is an officially registered tour operator/wholesaler with a history dating back to 1968, established as an independent company in 1990 in Mexico City and associated with the ROYALE Group since 2003. The company is a member of the National Chamber of Commerce (CANACO), the Mexican-German Chamber of Commerce and Industry (CAMEXA) and the Mexican Association of Travel Agents in Mexico City (AMAV CDMX). Our specialties are International Fairs, Conventions, Special Trips, Incoming Tourism (Mexico) and Travel Concierge Services.',
        'paragraph2' => 'KOMEX tours is the official travel agency in Mexico for the ITMA fair. We also cooperate closely with DE International (official representative of the fairgrounds of Cologne, Munich, Berlin, Nuremberg) and the representatives of the fairs of Frankfurt, Hannover and Cologne in Mexico.'
    ],
    'office' => [
        'title' => 'Our Offices',
        'paragraph1' => 'The Coronavirus Pandemic has changed the world, in particular global tourism as we knew it before. ',
        'paragraph2' => ' Many businesses have closed or are struggling to survive, including cruise lines, airlines, hotels, tour operators and travel agencies.  Despite or precisely because of this, travel agencies have proven their particular value to customers in times of crisis compared to online sales.',
        'paragraph3' => 'To move forward, the challenge is to reinvent the traditional business and adapt to the changes to come. At KOMEX tours we have taken this to heart, and turned our agency into a "Virtual Travel Advisory Boutique" for domestic clients. - What does this mean?',
        'paragraph4' => 'Boutique means that the range of travel services is very selective, high quality and reliable, safe, tailor-made and at fair prices, national and international. We focus on a personalised service for both corporate and private clients in Mexico. Our priority is customer service in all its aspects.',
        'paragraph5' => 'On the other hand, the limitation of a boutique also means that we are not an agency for all types of travel, i.e. we do not offer mass tourism or low budget trips nor do we want to compete with the lowest price on the market (internet platforms). We are not sales agents. We are professional travel advisers with more than 30 years of experience.',
        'paragraph6' => 'We work from home and advise our clients online by telephone as before. Yet we receive neither clients nor suppliers, everything is contactless at a healthy distance.',
        'paragraph7' => 'And the new thing is that we arrange virtual consultation appointments via Skype, Zoom or similar with all the modern technological support. Thanks to technology we can show you online documents, presentations, videos, etc. about destinations, hotels, attractions all over the world and itineraries to design tailor-made trips.',
        'paragraph8' => 'Within the CDMX we also offer an in-home consultancy service. Our consultants visit clients at their home upon request.',
        'paragraph9' => 'We have high technology support such as an online booking system, and we invest in a Backoffice and CRM system to increase our efficiency, which does not replace the human factor.',
        'paragraph10' => 'Our administration is under the published address, in charge of invoicing and online payment handling.',
        'paragraph11' => 'We have vision, mission and values. In short: Customer first.',
    ],
    'colaborators' => [
        'title' => 'Our partners',
        'card1' => [
            'name' => 'Suzanne C. Holst',
            'title' => 'Outgoing Sales and Trade Fairs Manager',
            'paragraph1' => 'She has more than 25 years in outgoing tourism and is a specialist in Europe, the Orient and South America. She lived and worked in Uruguay and 9 years in Germany, speaks fluently German, French and English. Her favorite hobby is traveling the world, which explains her eagerness to organize very special trips such as to India (2011, 2012, 2014, 2020) or Indochina (2013 and 2015),Kenya and Tanzania (2014), Portugal and Morocco (2015), Russia and Scandinavia (2016), South Africa and Namibia (2017).',
            'paragraph2' => 'Countries visited: Germany, France, Czech Republic, Austria, England, Italy, Spain, Portugal, Greece, Turkey, Jordan, Egypt, Morocco, India, Nepal, Thailand, Vietnam, Cambodia, Hong Kong, South Korea, Japan, Australia, New Zealand, Singapore, Philippines, Kenya, Tanzania, United States, Canada, Brazil, Uruguay, Argentina, Chile, Peru, Colombia, Venezuela, Bahamas, Virgin Islands, Dominican Republic among others and of course Mexico.',
        ],
        'card2' => [
            'name' => 'Sr. Hans-Peter Holst',
            'title' => 'General Management, International Sales.',
            'paragraph1' => 'The director of the company is a professional in tourism since 1974 with extensive experience in travel operations in Mexico, Europe (his native country: Germany) and the Americas. He speaks Spanish, English and Aleman. Specialist in organization of conventions and special groups, he knows Mexico as a tourist destination as well as his own country, and constantly promotes it in important fairs around the world, representing the ROYALE Group.',
            'paragraph2' => 'Countries visited: Sweden, Denmark, Norway, Iceland, Germany, Holland, Belgium, France, Czech Republic, Austria, Switzerland, England, Portugal, Spain, Italy, Greece, Turkey, United States, Canada, Brazil, Uruguay, Argentina, Chile, Peru, Venezuela, Dominican Republic, Cuba, Guatemala among others and of course most parts of Mexico between Tijuana and Cozumel.',
        ],
        'card3' => [
            'name' => 'Lic. Lucero Flores V.',
            'title' => 'Marketing Assistant/Internet',
            'paragraph1' => 'University degree in Administration of Leisure time specialized in tourism. She is a passionate of design and marketing and working as Assistant Manager of Marketing since 2016. Furthermore, she is the link to the ROYALE Group and coordinator of the internal activities of the group.',
            'paragraph2' => ''
        ],
        'final' => 'Our international sales partners',
    ],
    'competittions' => [
        'title' => 'Our Expertise',
        'paragraph' => [
            '1' => 'Business Trips to International Fairs worldwide and extensions for Exhibitors and Visitors',
            '2' => 'Cruises and Cultural Tours',
            '3' => 'Special trips for groups around the world',
            '4' => 'Professional Study Tours and Incentive Trips',
            '5' => 'Congresses, Conventions and Business Meetings',
            '6' => 'Inbound Tourism and Destination Management (DMC) in Mexico)',
            '7' => 'Travel Concierge Service (Lifestyle Management and Personal Assistance in Mexico City)',
        ]
    ],
    'royale' => [
        'title' => 'The ROYALE Group of Companies',
        'paragraph1' => 'The ROYALE Group is a Mexican group with activities in the tourism industry and has offices in Cancun/Riviera Maya, Cozumel, Chetumal/Costa Maya, Merida/Yucatan, Huatulco, Mexico City, Acapulco, Puerto Vallarta, Guadalajara, Los Cabos/Baja California, Monterrey among others with currently about 150 employees. In the beach destinations we are essentially dedicated to "Destination Management" (logistic services at the destination) and in CDMX to Inbound Tourism (operator/wholesaler of circuits and tours throughout Mexico), known mainly in the tourism industry.',
        'paragraph2' => 'Among others, the Group´s offices provide all tourism services and logistical support for tour operators, corporate clients and national and international associations. The ROYALE Group is currently implementing a new "Concierge Service" to offer the traveler a unique service at the destination (Lifestyle Management and Personal Assistance).',
        'paragraph3' => 'KOMEX tours/Grupo ROYALE promotes its services in Mexico in several international fairs e.g. WTM World Travel Market in London, the ITB Internationale Tourismus Börse in Berlin, IMEX in Frankfurt, IT&ME in Chicago or SEATRADE in Miami. As buyers we also visit fairs such as the annual Tianguis Turístico de México and IBTM of the Americas among others.',
    ],
    'questions' => [
        'title' => 'Frequent Questions',
        'points' =>  [
            '1' => [
                'title' => 'Are all Travel Agencies the same?',
                'paragraph1' => 'In fact, each Travel Agency is different and has different fields of expertise. Depending on the type of travel you are looking for, one agency will suit you better than another.'
            ],
            '2' => [
                'title' => 'Is it correct to say that no one uses Travel Agencies today?',
                'paragraph1' => 'Travel agencies currently still sell 51% of airline tickets, 87% of cruises, 45% of car rentals and approx. 47% of all hotels according to ASTA (American Society of Travel Agents).'
            ],
            '3' => [
                'title' => 'Have Travel Agencies lost their justification for existing?',
                'paragraph1' => 'Travel agents book hotels, cruises and activities every day. Suppliers of these services know this and are more willing to grant exceptions for Travel Agents.'
            ],
            '4' => [
               'title' => 'Isnt the need to use Travel Agents in the process of being replaced by the Internet?',
                'paragraph1' => 'There are some things that technology cannot replicate and the personal touch is one of them. The Internet is a valuable resource, but it cannot substitute for the expertise, guidance and personal service of a travel agent. When travelers are stressed by tight schedules, travel agents have all the information at their fingertips, saving valuable hours on the Internet. Agents can also offer recommendations usually based on their own experiences.'
            ],
            '5' => [
                'title' => 'Do Travel Agencies have up-to-date information like the Internet?',
                'paragraph1' => 'Travel Agencies get some of their information from the same sources as OTA (online travel agency) sites. They also receive daily mailings and notices about the latest resort and hotel specials that are not always posted on the Internet. As well, they can call a place directly to see if they can get plus specials for you, which an online site cannot do.'
            ],
            '6' => [
                'title' => 'Can Travel Agencies only book my flight and hotel?',
                'paragraph1' => 'It is a fact that travel agents can arrange car services, customized tours, activities and insurance; in addition to all the basic travel services. They are also an excellent source of information regarding recommended restaurants, places to visit, and packing tips.'
            ],
            '7' => [
                'title' => 'How does KOMEX tours differ from other agencies?',
                'paragraph1' => 'We are proud to offer our clients a comprehensive, high quality service with personalized attention with over 25 years of experience in worldwide travel. We are registered in the National Tourism Registry (No. 04090141369) and we have the Distintivo M certification. Our specialties are tailor-made trips for exhibitors and visitors of international fairs in the 5 continents, individual extensions, private trips for groups and luxury; as well as tourist services in Mexico and the Mayan World (incoming tourism), such as individual and group trips, cultural circuits, incentives and conventions. KOMEX tours is a company of the ROYALE Group with partners in the main destinations of Mexico, who personally provide the incoming service to the client.',
                'paragraph2' => 'I can easily book my own trip without using a Travel Agency. Whats the difference? Travel Agencies in addition to having access to all the sites you would use in your search to book travel, also have access to quotes and special deals for packages, which are often not available to the public. Your Travel Agent has more opportunities to help you in some situations, such as: when you find hotels that claim to be fully booked, for example, for agency bookings they may have vacant rooms. So, if someone says "NO" to you, your Travel Agent can assist in turning it into a "YES"',
            ],
            '8' => [
                'title' => 'Isnt it more expensive to use a Travel Agent?',
                'paragraph1' => 'Service charges really depend on each Agency. While some of the more upscale agencies have higher charges, the average charge is relatively on the low end. Some agencies are willing to waive or offer a discount once you have completed your trip with them. Also, you can ask beforehand how much they charge and decide if it is worth it. It is also important to remember that many online websites such as. Expedia, Best Day, Price Travel and many other suppliers charge service fees.',
            ],
            '9' => [
                'title' => 'Do Travel Agencies abuse when they dont offer me the cheapest price?',
                'paragraph1' => 'Travel agents know the advantages and disadvantages of different itineraries. You may find a cheaper one, but this difference could cause you a lot of inconvenience. For example, forced longer stays at the airport and/or inconvenient travel schedules. A good Travel Agent will try to give you the best value for your money, including the most direct connections and most convenient itineraries they can find.',
            ],
            '10' => [
                'title' => '¿No es una pérdida de tiempo de consultar una Agencia de Viajes?',
                'paragraph1' => 'A pesar de que puede encontrar por usted mismo mucha de la información que el Agente de Viajes le provee, ahorraría mucho de su valioso tiempo. Un Agente de Viajes le ahorra horas de búsquedas pesadas y comparativos de precios de compra. Cuenta con tarifas actualizadas, condiciones de hoteles e interesantes actividades, además de que su conocimiento y experiencia le dan la ventaja en la planeación de viajes.'
            ],
            '11'=> [
                'title' => 'Why should I use a Travel Agency?',
                'paragraph1' => 'Travel Agency has many advantages for the traveler:',
                'paragraph2' => 'CUSTOMERS ATTORNEY: If you have a particular problem within your trip, the Agent is on your side to act on your behalf and get you restitution.',
                'paragraph3' => 'PROFESSIONAL ADVICE: Travel Agents exist to ensure what you get, where and when you want to travel and the most favorable price.',
                'paragraph4' => 'ADVICE FROM AN EXPERT: Travel Agents are experts in understanding travel information and codes',
                'paragraph5' => 'IMPARTIAL INFORMATION: Agents work for their customers, not their suppliers. And they know that a happy customer will be a repeat customer.',
                'paragraph6' => 'SAFETY: A Travel Agency is registered and subject to the laws of the country, both consumer protection and fiscal. The laws in Mexico are different and very different for travel agencies/operators, carriers and hotels.',
                'paragraph7' => 'PERSONALIZED SERVICE: Instead of an impersonal voice thousands of miles away, Travel Agents are your "neighbors" and know what you are looking for and value in your travel experiences.',
                'paragraph8' => 'TIME: Instead of checking a long list of travel websites, which only offer rates and prices for companies that have contracts with them, why not go directly to the original source? A travel agent has all the information at hand, saving you hours in front of your computer monitor.',
            ],
            '12' => [
                'title' => 'What benefits does KOMEX tours give me as a customer?',
                'paragraph1' => 'At KOMEX tours we are committed to our clients and not to any particular supplier. With honesty and transparency we advise, coordinate, organize and book all the details of the trip. We put on the client´s shirt and treat their trip as we would a tour for ourselves. Our services include both pre- and post-sales service, as well as 24/7 emergency assistance during your trip and assistance in case of changes and cancellations; and naturally final prices and full tax invoicing. Our responsibility and clear conditions guarantee long-lasting friendships. We want our customers to be satisfied, to come back and to recommend us.'
            ],
            '13' => [
                'title' => 'What are the office hours of KOMEX tours?',
                'paragraph1' => 'We work Monday to Friday from 08:30 to 19:00 hrs, and Saturday from 09:00 to 13:00 hrs. In case of emergency our management is available 24 hours a day.',
            ],
        ]
    ],
    'best' => [
        'title' => 'Best Practices',
         'points' => [
             '1' => [
                 'paragraph1' => 'During the Coronavirus pandemic:',
                 'paragraph2' => 'Payments by credit card or wire transfer in MXN and USD',
                 'paragraph3' => 'Travel Assistance and Protection Insurance Covid-19',
                 'paragraph4' => 'Familiarization with the terms and conditions of our providers',
                 'paragraph5' => 'Up-to-date information from official sources',
                 'paragraph6' => 'Communication documentation',
                 'paragraph7' => 'Flexible cancellation policies',
                 'paragraph8' => 'Personalized telephone service',
                 'paragraph9' => 'Extended business hours',
                 'paragraph10' => '24/7 Emergency Service',
                 'paragraph11' => 'We keep our promises',
                 'paragraph12' => 'On-site assistance and support service',
             ],
             '2' => [
                'title' => 'Added Values',
                 'paragraph1' => 'Final and guaranteed prices (no hidden extra charges)',
                 'paragraph2' => 'Useful recommendations for travelers',
                 'paragraph3' => 'Travel Insurance',
                 'paragraph4' => 'Destination Information ',
                 'paragraph5' => 'Extensions',
                 'paragraph6' => 'Legal security',
                 'paragraph7' => 'Clear conditions according to international standards',
                 'paragraph8' => 'Payment facilities',
                 'paragraph9' => 'Fiscal Invoices',
                 'paragraph10' => 'Courier Service',
                 'paragraph11' => 'Assistance with changes, cancellations, complaints and refund procedures',
             ]
         ]
    ]
];
