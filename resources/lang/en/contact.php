<?php

return [
    'title' => 'Contact',
    'slogan' => 'Distance prevents us from seeing you, but not from being by your side',
    'mailboxes' => '',
    'telephoneswitch' => 'Telephone Switchboard ',
    'admin' => 'Administration',
    'lada' => 'Toll free:',
    'fairs' => 'International Fairs:',
    'incoming' => 'Incoming Mexico/DMC:',
    'whatsapp' => 'After hours emergencies y WhatsApp',
    'email' => 'Email:',
    'general' => 'KOMEX tours general:',
    'fairsandextensions' => 'Ferias y extensiones:',
    'specialtrips' => 'Special Trips:',
    'passion' => 'Passion for Mexico:',
    'destination' => 'Destination Management:',
    'conven' => 'Conventions & Incentives:',
    'locate' => 'Location:',
    'form' => [
        'name' => 'Name',
        'telephone' => 'Telephone',
        'email' => 'Email',
        'message' => 'Message',
    ],
];
