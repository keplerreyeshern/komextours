<?php

return [
    'more' => 'Read More',
    'testimonials' => [
        'title' => 'Testimonials',
    ],
    'blog' => [
        'title' => 'Blog',
    ],
    'news' => [
        'title' => 'News',
    ],
    'show' => [
        'date' => 'Date:'
    ],
];
