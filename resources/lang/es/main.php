<?php

return [
    'more' => 'Ver más',
    'fairs' => [
        'title' => 'Ferias Internacionales',
        'content' => 'Consulta y facilidades de arreglos especiales para expositores y visitantes de ferias internacionales',
    ],
    'extensions' => [
        'title' => 'Extensiones',
        'content' => 'Dependiendo de la sede de una feria ofrecemos extensiones a la medida',
    ],
    'tours' => [
        'title' => 'Viajes a la medida',
        'content' => 'Organizamos viajes para grupos a la medida a destinos nacionales e internacionales',
    ],
    'conve' => [
        'title' => 'Convenciones',
        'content' => 'Consulta y facilidades de arreglos especiales para expositores y visitantes de ferias internacionales',
    ],
    'fairsnext' => [
        'title' => 'Próximas Ferias Internacionales',
        'star' => 'Inicio',
        'place' => 'Ubicación',
    ]

];
