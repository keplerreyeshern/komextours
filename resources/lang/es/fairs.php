<?php

return [

    'title' => 'Ferias',
    'description' => '¿Sabía usted que viajar a una Feria Internacional implica hasta 20 o más diferentes negociaciones con proveedores y que nosotros le simplificamos todo el proceso, con una asesoría personalizada que le proporcionaremos? ',
    'collapse' => [
        'one' => 'Experiencia probada de más de 14 años',
        'two' => 'Conocemos básicamente las ferias, el lugar, los hoteles; tenemos un know-how completo de todo lo que implica una feria',
        'three' => 'Le aseguramos su lugar en la feria y en hoteles cercanos',
        'four' => 'Tenemos alianzas con los organizadores de las principales Ferias Internacionales',
        'five' => 'Le damos una solución integral a su viaje de negocios y le podemos organizar su Extensión Turística desde el lugar de la feria',
    ],
    'search' => 'Busqueda por Nombre',
    'select' => 'Selecciona un Ramo',
    'date' => 'Fecha:',
    'place' => 'Ubicación:',
    'sector' => 'Ramo:',
    'more' => 'Más Información',
    'empty' => 'No encontraron ferias con esos parametros',
    'show' => [
        'sector' => 'Ramo',
        'date' => 'Fecha',
        'facilities' => 'Facilidades:',
        'rates' => 'Tarifas:',
        'services' => 'Servicios',
        'description' => 'Descripción',
        'inclusions' => 'Inclusiones',
        'place' => 'Ubicación',
        'leagues' => 'Ligas',
        'offers' => [
            'title' => 'Ofertas',
            'empty' => 'Proximamente aquí encontraras las ofertas más interesantes relacionadas con esta feria',
        ],
    ],
];
