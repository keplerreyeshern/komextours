<?php

return [
    'news' => [
        'principal' => '/noticias',
    ],
    'testimonials' => '/testimoniales',
];
