<?php

return [

    'group' => 'Grupo Royale',
    'news' => 'Noticias',
    'blog' => 'Blog',
    'offers' => 'Ofertas',
    'testimonials' => 'Testimoniales',
    'global' => 'Condiciones Generales',
    'service' => 'Menu de Servicios',

];
