<?php

return [
    'street' => 'Calle:',
    'suburb' => 'Col.',
    'side' => 'Lada sin costo:',
    'telephone' => 'Tel (Conmutador)',
    'emergencies' => 'Emergencias',
    'follow' => 'Siguenos en:',
    'copyright' => 'Todos los derechos reservados.',
    'privacy' => 'Aviso de Privacidad',
    'update' => 'Ultima Actualización',
];
