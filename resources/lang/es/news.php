<?php

return [


    'more' => 'Leer Más',
    'testimonials' => [
        'title' => 'Testimoniales',
    ],
    'blog' => [
        'title' => 'Blog',
    ],
    'news' => [
        'title' => 'Noticias',
    ],
    'show' => [
        'date' => 'Fecha:'
    ],

];
