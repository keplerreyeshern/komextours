<?php

return [

    'component' => [
        'all' => 'Ver Todos',
        'title1' => 'Luna, México - Viaje especial a La india & Nepal (2012)',
        'content1' => '“El viaje me llevó a confrontarme con una explosión fantástica de colores, deliciosos y nuevos sabores así como olores, todos llevados a su máxima expresión. Observar, participar y respetar otras religiones y formas diferentes de pensar que ven la vida de una manera tan peculiar y distinta de la mía. Visitar los más exquisitos, imponentes, majestuosos y esplendidos palacios, templos y fuertes transitados a lo largo del viaje, me evocaron muy diferentes y placenteros sentimientos y emociones.
                        El sobrevuelo de los Himalayas, me dejó sin aliento, sin palabras, llevándome a la cima del mundo y de mis más anhelados sueños. La gente común, siempre sonriente y amable, me hizo pensar que un mundo de Paz es posible.
                        Nuestros maravillosos compañeros de viaje, siempre atentos, cordiales y dispuestos a disfrutar el día a día, hicieron posible que el viaje fuera todo un éxito. Suzanne y Betty, que no solo organizaron el viaje estupendamente con la más alta calidad, sorpresas y muchos detalles, sino que además, gozaron y disfrutaron con nosotros cada momento, a ambas, les doy las Gracias, por haber hecho de este viaje a la India simplemente”',
        'title2' => 'Sr. L. Castro, Waltvick de México Viaje Feria con extensión (Noviembre 2013)',
        'content2' => '“Todo estuvo excelente, en general tanto los hoteles como los traslados bastante bien! Seguiremos en contacto para futuros eventos.”',
        'title3' => 'E. Romero, Impulsora Euro, México - Viaje Feria con extension (Enero 2014)',
        'content3' => '“Nos fue muy bien. Nos encantó la recomendación de Kitzbuhel e Innsbruck. Los hoteles muy bien.”',
        'title4' => 'P. González, Optical Growging, Mexico - Viaje Feria con extensión (Febrero 2014)',
        'content4' => '“Nos fue muy bien, la verdad todo estuvo excelente. Los hoteles mega limpios y con todo lo necesario, los tours muy padres, quedamos muy contento con el servicio brindado.”',
        'title5' => 'G. González, DE International de México - Viaje individual (Febrero 2014)',
        'content5' => '“El tour que nos organizaste con la agencia de viajes local, fue excelente, así como los conocimientos de historia, arte y del idioma Español del guía Oleg. Muchas gracias nuevamente y reiteramos que fue un viaje muy bien montado gracias a los tours y recomendaciones por parte de KOMEX Royale tours.”',
        'title6' => 'Mohamed Amr, Executive Manager, Decastro Tours, Egypt - Luna de miel en México (Octubre 2013)',
        'content6' => '“Greetings from Decastro Tours Egypt. Last time I booked through you was for my honeymoon which was great and had a lovely choice in Cancun and everything was perfect from you and your staff. Thank you,”',
        'title7' => 'Sophie Wright, Operations Supervisor, Martin Randall Travel Ltd, UK - Visita de México (Noviembre 2013)',
        'content7' => '“Thank you very much for arranging my trip to Mexico - I had a great time and have now returned to the office to work on an itinerary.”',
        'title8' => 'Jimmy Liu, CBT Holidays, Australia - Grupo en circuito por México (Diciembre 2013)',
        'content8' => '“The trip was great and guides are good! Just return back Sydney and looking the group going in April.”',
        'title9' => 'Božena Teissingová, White Grant s.r.o., Rep. Checa - Grupo en circuito por México (Abril 2014)',
        'content9' => '“Quisiera darte muchas gracias por todo que hiciste para mi grupo, especialmente por Lula. No he encontrado muchas guías como ella. Es excelente y nos dio mucho placer de conocer-la. Todos los clientes estuvieron muy contentos, México les gustó muchísimo y sabes como es. Si los clientes están contentos, es el mejor premio que podemos obtener. Espero que habrá más turistas para México en el futuro aunque somos una agencia pequeña con los clientes permanentes que ya hacen planes para otro viaje a otro país. Muchas gracias de nuevo y mando un cordial saludo de Praga a todos de la oficina.”',
        'title10' => 'Sra. N. Aizpuru - Viaje especial a Portugal y Marruecos (Abril 2015)',
        'content10' => '“La verdad que quedé encantada con este viaje y con ustedes! A quien le platico,  no dejo de echarles porras y es la pura verdad.  Yo había jurado nunca mas ir en tour a Europa después de una amarga experiencia hace 30 y pico de años pero quedé fascinada. Excelentes tours, excelente organización, excelentes hoteles y restaurantes pero sobretodo excelentes ustedes como personas y un gran grupo“',
        'title11' => 'Sra. M. Sánchez- Viaje a Portugal y Marruecos (Abril 2015)',
        'content11' => '“Gracias mis queridas Bety y Suzanne. Con su profesionalismo, cariño y gran pasión por su trabajo, han sido el eje y alma de este entrañable grupo de viaje que tanto nos ha enriquecido al hacer más disfrutable estos maravillosos viajes.”',
    ],

];
