<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'KomexTours') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    @yield('codes')
</head>
<body>
    <div id="fb-root"></div>
    @if(App::islocale('es'))
        <script async defer src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2"></script>
    @endif
    @if(App::islocale('en'))
        <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
    @endif

    <div id="app">
        @include('public.layouts.header')
        @yield('content')
        <div class="btn-whatsapp">
            <a href="https://api.whatsapp.com/send?text=Hola, Solicito una cotización &phone=525554074786" target="_blank" (click)="clickWP('whatsapp')">
                <img src="http://s2.accesoperu.com/logos/btn_whatsapp.png" alt="">
            </a>
        </div>
        @include('public.layouts.footer')
    </div>
</body>
</html>
