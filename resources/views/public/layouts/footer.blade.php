<div class="footer-dark footer-color">
    <h1 class="text-center text-md-left">KOMEX tours</h1>
    <div class="row d-flex align-items-center">
        <div class="col-12 col-md-3 d-flex justify-content-center justify-content-md-start">
            <p>
                @lang('footer.street') La Mesa 4 int. 4,
                <br>@lang('footer.suburb')  Santa Ursula Xitla,
                <br> CP.14420 Tlalpan,
                <br> Ciudad de México, MÉXICO
            </p>
        </div>
        <div class="col-12 col-md-6 d-flex justify-content-center">
            <p>@lang('footer.telephone') +52 55 5615 8741
                <br>@lang('footer.side') 800 849 1514
                <br>@lang('footer.emergencies') 55 5453 0398, 55 5407 4786</p>
        </div>
        <div class="col-12 col-md-3 d-flex justify-content-center justify-content-md-start">
            <p>Mail: komex@royaletours.com.mx
                <br>Skype:
                <a href="skype:hpholst1?chat" onclick="return skypeCheck();" target="_blank">
                    hpholst1,
                </a>
                <a href="skype:suzanneholst1?chat" onclick="return skypeCheck();" target="_blank">
                    suzanneholst1
                </a>
            <br>
                @lang('footer.follow')
                <a href="https://www.facebook.com/komextours" target="_blank">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="https://twitter.com/KomexTOURS1" target="_blank">
                    <i class="fab fa-twitter"></i>
                </a>
            </p>
        </div>
    </div>
</div>
<div class="footer-light">
    <div class="row">
        <div class="col-12 col-md-6">
            <p>KomexTours ® 2014. @lang('footer.copyright')</p>
        </div>
        <div class="col-12 col-md-6">
            <p class="align-right"> <a href="{{route('noticeOfPrivacy')}}">@lang('footer.privacy')</a> | @lang('footer.update')  {{ \Carbon\Carbon::parse(date_create('now'))->translatedFormat('F Y') }}</p>
        </div>
    </div>
</div>
