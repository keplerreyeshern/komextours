{{--<div class="hamburger-menu" onclick="show()">--}}
{{--    <div class="bar" id="btn-sidebar"></div>--}}
{{--</div>--}}
<div class="col-6 col-md-12 offset-3 offset-md-0 bg-info sidebar hide-sidebar -bor" id="sidebar" >
    <ul class="btns-leagues">
        <li>
            <a href="http://www.gruporoyale.com" target="_blank">
                <img src="{{ asset('images/royale-page.jpeg')  }}" class="img-fluid w-100" alt="royale">
            </a>
        </li>
        <li>
            <a href="http://mexiko.ahk.de/es" target="_blank">
                <img src="{{ asset('images/camexa-page.jpeg') }}" class="img-fluid w-100" alt="camexa">
            </a>
        </li>
        <li>
            <a href="http://royaledmc.com" target="_blank">
                <img src="{{ asset('images/destination-manager-page.jpeg') }}" class="img-fluid w-100" alt="destination">
            </a>
        </li>
        <li>
            <a href="https://www.ccmexico.com.mx/es/" target="_blank">
                <img src="{{ asset('images/canaco-cdmx-logo.jpg') }}" class="img-fluid w-100" alt="conaco">
            </a>
        </li>
    </ul>
    @if($fair)
        <div class="row">
            <div class="col-12">
                @foreach($fair as $fa)
                    <a href="{{ route('event', ['slug' => $fa->slug] ) }}">
                        @if($fa->poster)
                            <img src="{{asset($fa->poster)}}" class="img-fluid w-100 h-100" alt="{{$fa->poster}}" id="poster">
                        @else
                            <img src="{{asset('images/missing.png')}}" class="img-fluid w-100 h-100" alt="Sin imagen" id="poster">
                        @endif
                    </a>
                @endforeach
            </div>
        </div>
    @endif
    <ul class="btns-internal">
        @foreach($managements as $management)
            <li><a href="{{ $management->url }}" target="_blank">@lang('sidebar.news')</a></li>
        @endforeach
        <li><a href="{{ route('blogs') }}">@lang('sidebar.blog')</a></li>
{{--        <li><a href="{{ route('offers') }}">@lang('sidebar.offers')</a></li> --}}
        <li><a href="{{ route('testimonials') }}">@lang('sidebar.testimonials')</a></li>
        <li><a target="_blank" href="{{asset('docs/condiciones-generales.pdf')}}">@lang('sidebar.global')</a></li>
        <li><a target="_blank" href="{{asset('docs/menu-de-servicios.pdf')}}">@lang('sidebar.service')</a></li>
    </ul>
</div>
<script type="text/javascript">
    var showMenu = false;
    function show(){
        if (showMenu){
            document.getElementById('sidebar').classList.add('hide-sidebar');
            document.getElementById('sidebar').classList.remove('show-sidebar');
            document.getElementById('btn-sidebar').classList.remove('animate');
            showMenu = false;
        } else {
            document.getElementById('sidebar').classList.add('show-sidebar');
            document.getElementById('sidebar').classList.remove('hide-sidebar');
            document.getElementById('btn-sidebar').classList.add('animate');
            showMenu = true;
        }
    }
</script>
