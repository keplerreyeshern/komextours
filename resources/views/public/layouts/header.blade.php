<div class="container-fluid bg-social">
    <div class="row">
        <div class="col-12">
            <div class="barSocialNetworks">
                <ul>
                    <li>
                        @if (App::isLocale('en'))
                            <a href="{{ url('lang', ['es']) }}">
                                <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" width="20px" alt="es">
                            </a>
                        @elseif (App::isLocale('es'))

                            <a href="{{ url('lang', ['en']) }}">
                                <img src="{{ asset('images/flags/Flag_of_United.svg') }}" width="20px" alt="en">
                            </a>
                        @endif
                    </li>
                    <li>
                        <a href="https://www.facebook.com/komextours" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/KomexTOURS1" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="col-10 offset-md-1">
        <div class="row d-flex align-items-center">
            <div class="col-8">
                <a href="{{route('index')}}">
                    <img src="{{asset('images/komextours.png')}}" class="img-fluid logo-komex" alt="Komex tours">
                </a>
            </div>
            <div class="col-4">
                <div class="row d-flex align-items-center">
                    <div class="col-6">
                        <img src="{{asset('images/royal.png')}}" class="img-fluid logo-komex" alt="Royal">
                    </div>
                    <div class="col-6">
                        <img src="{{asset('images/safeTravels.png')}}" class="img-fluid logo-komex" alt="Safe Travels">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav>
    <div class="hamburger-menu" onclick="Menu()">
        <div class="bar" id="btn-menu"></div>
    </div>
    <ul class="hide" id="menu">
        <li><a class="text-capitalize font-size-menu" href="{{ route('about') }}">@lang('menu.about')</a></li>
        <li><a class="text-capitalize font-size-menu" href="{{ route('services', ['item' => ' ']) }}">@lang('menu.services')</a></li>
        <li><a class="text-capitalize font-size-menu" href="{{ route('events') }}">@lang('menu.fairs')</a></li>
        <li><a class="text-capitalize font-size-menu" href="{{ route('extensions') }}">@lang('menu.extension')</a></li>
        <li><a class="text-capitalize font-size-menu" href="{{ route('mexico') }}">@lang('menu.mexico')</a></li>
        <li><a class="text-capitalize font-size-menu" href="{{ route('specialTrips') }}">@lang('menu.specials')</a></li>
        <li><a class="text-capitalize font-size-menu" href="{{ route('contact') }}">@lang('menu.contact')</a></li>
    </ul>
</nav>
@include('public.main.components.banner')
<script type="text/javascript">
    var showMenu = false;
    function Menu(){
        if (showMenu){
            document.getElementById('menu').classList.add('hide');
            document.getElementById('menu').classList.remove('show');
            document.getElementById('btn-menu').classList.remove('animate');
            showMenu = false;
        } else {
            document.getElementById('menu').classList.add('show');
            document.getElementById('menu').classList.remove('hide');
            document.getElementById('btn-menu').classList.add('animate');
            showMenu = true;
        }
    }
</script>
