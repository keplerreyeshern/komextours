@extends('public.layouts.app')

@section('head')
    <meta property="og:url"           content="{{ config('app.url') }}/eventos/{{$fair->slug}}" />
    <meta property="og:type"          content="Feria" />
    <meta property="og:title"         content="{{$fair->name}}" />
    <meta property="og:description"   content="{!! $fair->short_description !!}" />
    <meta property="og:image"         content="{{ config('app.url') }}/{{$fair->logo}}" />
@endsection

@section('styles')
    <style type="text/css">
        #map {
            width:100%;
            height: 500px;
        }
    </style>
@endsection

@section('content')
    <div id="fb-root"></div>
    @if(App::islocale('es'))
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v9.0&appId=463005901528543&autoLogAppEvents=1" nonce="ykb4vpSp"></script>
    @endif
    @if(App::islocale('en'))
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0&appId=463005901528543&autoLogAppEvents=1" nonce="ykb4vpSp"></script>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-12 mt-2">
                <h1 class="text-success titles">{{$fair->name}}</h1>
                @if($fair->logo)
                    <a data-fancybox="gallery" href="{{ $fair->logo }}">
                        <img src="{{asset($fair->logo)}}" class="img-fluid w-25 image" alt="{{$fair->logo}}" id="logo">
                    </a>
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-25 image" alt="Sin imagen" id="poster">
                @endif
                <p>
                    <span class="title-card font-bold">@lang('fairs.show.sector'):</span>
                    {{ $fair->sector->name }}
                </p>
                <p>
                    <span class="title-card font-bold">@lang('fairs.show.date'): </span>
                    <span class="tex-capitalize">
                        {{ \Carbon\Carbon::parse($fair->startDate)->translatedFormat('d F Y') }} a
                        {{ \Carbon\Carbon::parse($fair->endDate)->translatedFormat('d F Y') }}
                    </span>
                </p>
                @if(!empty($fair->description))
                    <p class="title-card font-bold">@lang('fairs.show.description')</p>
                    <p>{!! $fair->description !!}</p>
                @endif
                @if(!empty($fair->services))
                    <p class="title-card font-bold">@lang('fairs.show.services')</p>
                    <p>{!! $fair->services !!}</p>
                @endif
                <div class="container mt-3">
                    <div class="row d-flex justify-content-center">
                        @foreach($images as $keyg => $gallery)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <a data-fancybox="gallery" href="{{ $gallery->name }}">
                                    <img src="{{asset($gallery->name)}}" class="img-fluid imagae" alt="{{$gallery->name}}" id="Imagen de galeria {{ $keyg + 1 }}">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <p>
                    <span class="title-card font-bold">@lang('fairs.show.place'):</span>
                    <span class="font-bold">{{$fair->place}}</span>
                </p>
                    <div class="row">
                        <div class="col-12 mt-5" id="map"></div>
                    </div>
{{--                <p class="title-card font-bold mt-3 ">@lang('fairs.show.leagues')</p>--}}
{{--                    <ul class="border p-3">--}}
{{--                        @forelse($fair->leagues->sortby('name') as $key => $league)--}}
{{--                            <li>--}}
{{--                                <a href="{{ $league->url }}" class="link-league">--}}
{{--                                    {{ $league->name }}--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @empty--}}
{{--                            <p>No tiene ninguna liga asociada</p>--}}
{{--                        @endforelse--}}
{{--                    </ul>--}}
{{--                <p class="title-card font-bold">@lang('fairs.show.offers.title')</p>--}}
                <div class="row my-4">
                    <div class="col-12">
                        @component('public.main.components.quotation')
                            @slot('page', 'fairs')
                            @slot('reference', $fair->id)
                        @endcomponent
                    </div>
                </div>
                @if(App::islocale('es'))
                    <iframe src="https://www.facebook.com/plugins/like.php?locale=es_LA&href={{ config('app.url') }}%2Feventos%2F{{$fair->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    <iframe src="https://www.facebook.com/plugins/share_button.php?locale=es_LA&href={{ config('app.url') }}%2Feventos%2F{{$fair->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                @endif
                @if(App::islocale('en'))
                    <iframe src="https://www.facebook.com/plugins/like.php?locale=en_US&href={{ config('app.url') }}%2Feventos%2F{{$fair->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    <iframe src="https://www.facebook.com/plugins/share_button.php?locale=en_US&href={{ config('app.url') }}%2Feventos%2F{{$fair->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                @endif

            </div>
        </div>
    </div>
    @php
        $location = $fair->location;
        $pos1 = strpos($location, ',');
        $pos1 = $pos1 + 1;
        $rest = substr($location, $pos1);
        $pos2 = strpos($rest, ',');
        $address = substr($rest, 0 ,$pos2);
    @endphp
    <input type="hidden" id="hidden_latitude" value="{{$fair->latitude}}">
    <input type="hidden" id="hidden_longitude" value="{{$fair->longitude}}">
    <input type="hidden" id="hidden_address" value="{{$address}}">
@endsection
@section('javascript')
    <script type="text/javascript">
        var map;
        var geocoder;
        var latitude = document.getElementById("hidden_latitude").value;
        var longitude = document.getElementById("hidden_longitude").value;
        var address = document.getElementById("hidden_address").value;

        function initMap() {
            var mapLayer = document.getElementById("map");
            var centerCoordinates = new google.maps.LatLng(latitude, longitude);
            var defaultOptions = { center: centerCoordinates, zoom: 8 }

            map = new google.maps.Map(mapLayer, defaultOptions);
            geocoder = new google.maps.Geocoder();

            geocoder.geocode( { 'address': address }, function(LocationResult, status) {
                // if (status == google.maps.GeocoderStatus.OK) {
                //     var latitude = LocationResult[0].geometry.location.lat();
                //     var longitude = LocationResult[0].geometry.location.lng();
                // }
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: 'KomexTours',
                    draggable: true,
                    clickable: true,
                    animation: google.maps.Animation.DROP
                });
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8_8kSrBymZR_OLHLeCfWZFrX7l1HNUTE&callback=initMap"></script>
@endsection
