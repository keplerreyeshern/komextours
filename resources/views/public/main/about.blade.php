@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-capitalize text-success font-weight-bold titles">@lang('about.title')</h1>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="true">@lang('about.history.title')</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="office-tab" data-toggle="tab" href="#office" role="tab" aria-controls="office" aria-selected="false">@lang('about.office.title')</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="colaborators-tab" data-toggle="tab" href="#colaborators" role="tab" aria-controls="colaborators" aria-selected="false">@lang('about.colaborators.title')</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="competittions-tab" data-toggle="tab" href="#competittions" role="tab" aria-controls="competittions" aria-selected="true">@lang('about.competittions.title')</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="royale-tab" data-toggle="tab" href="#royale" role="tab" aria-controls="royale" aria-selected="false">@lang('about.royale.title')</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="questions-tab" data-toggle="tab" href="#questions" role="tab" aria-controls="questions" aria-selected="false">@lang('about.questions.title')</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="best-tab" data-toggle="tab" href="#best" role="tab" aria-controls="best" aria-selected="false">@lang('about.best.title')</a>
                    </li>
                </ul>
                <div class="tab-content p-3" id="myTabContent">
                    <div class="tab-pane active" id="history" role="tabpanel" aria-labelledby="history-tab">
                        <p class="text-left">
                            @lang('about.history.paragraph1')<br><br>
                            @lang('about.history.paragraph2')
                        </p>
                        <div class="row align-items-center justify-content-center">
                            <div class="col-3">
                                <img src="{{ asset('images/marca/Camexa-logo.jpeg') }}" class="img-fluid w-100" alt="camexa">
                            </div>
                            <div class="col-3">
                                <img src="{{ asset('images/marca/canaco-cdmx-logo.png') }}" class="img-fluid w-100" alt="canaco">
                            </div>
                            <div class="col-3">
                                <img src="{{ asset('images/Registro-Nacional-de-Turismo-en-México.jpg') }}" class="img-fluid w-100" alt="rntm">
                            </div>
                            <div class="col-3">
                                <img src="{{ asset('images/marca/Logo_AMAVCDMX.png') }}" class="img-fluid w-100" alt="amavcdmx">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="office" role="tabpanel" aria-labelledby="office-tab">
                        <p class="text-left">
                            @lang('about.office.paragraph1')
                            @lang('about.office.paragraph2')
                            <br><br>
                            @lang('about.office.paragraph3')
                        </p>
                        <ol>
                            <li>
                                @lang('about.office.paragraph4')<br><br>
                                @lang('about.office.paragraph5')
                            </li>
                            <br>
                            <li>@lang('about.office.paragraph6')</li>
                            <br>
                            <li>@lang('about.office.paragraph7')</li>
                            <br>
                            <li>@lang('about.office.paragraph8')</li>
                            <br>
                            <li>@lang('about.office.paragraph9')</li>
                            <br>
                            <li>@lang('about.office.paragraph10')</li>
                            <br>
                            <li>@lang('about.office.paragraph11')</li>
                            <br>
                        </ol>
                    </div>
                    <div class="tab-pane " id="colaborators" role="tabpanel" aria-labelledby="colaborators-tab">
                        <div class="row px-1 ">
                            <div class="col-3">
                                <img src="{{ asset('images/faces/sussan.jpeg') }}" class="img-fluid w-100" alt="Suzanne C. Holst">
                            </div>
                            <div class="col-9">
                                <h3 class="text-info">@lang('about.colaborators.card1.name')</h3>
                                <h3 class="text-success">@lang('about.colaborators.card1.title')</h3>
                                <p class="text-left">
                                    @lang('about.colaborators.card1.paragraph1')
                                </p>
                            </div>
                        </div>
                        <div class="row px-1">
                            <div class="col-12">
                                <p class="text-left">
                                    @lang('about.colaborators.card1.paragraph2')
                                </p>
                            </div>
                        </div>
                        <div class="row px-1 mt-3">
                            <div class="col-3">
                                <img src="{{ asset('images/faces/Hans-Peter-Holst.jpeg') }}" class="img-fluid w-100" alt="Suzanne C. Holst">
                            </div>
                            <div class="col-9">
                                <h3 class="text-info">@lang('about.colaborators.card2.name')</h3>
                                <h3 class="text-success">@lang('about.colaborators.card2.title')</h3>
                                <p class="text-left">
                                    @lang('about.colaborators.card2.paragraph1')
                                </p>
                            </div>
                        </div>
                        <div class="row px-1">
                            <div class="col-12">
                                <p class="text-left">
                                    @lang('about.colaborators.card2.paragraph2')
                                </p>
                            </div>
                        </div>
                        <div class="row px-1 mt-3">
                            <div class="col-3">
                                <img src="{{ asset('images/faces/lucero-flores.jpeg') }}" class="img-fluid w-100" alt="Suzanne C. Holst">
                            </div>
                            <div class="col-9">
                                <h3 class="text-info">@lang('about.colaborators.card3.name')</h3>
                                <h3 class="text-success">@lang('about.colaborators.card3.title')</h3>
                                <p class="text-left">
                                    @lang('about.colaborators.card3.paragraph1')
                                </p>
                            </div>
                        </div>
                        <div class="row px-1">
                            <div class="col-12">
                                <p class="text-left">
                                    @lang('about.colaborators.card3.paragraph2')
                                </p>
                            </div>
                        </div>
                        <div class="row px-1">
                            <div class="col-12 d-flex justify-content-center">
                                <h3 class="text-end text-capitalize">@lang('about.colaborators.final')</h3>
                            </div>
                            <div class="col-12 d-flex justify-content-center">
                                <img src="{{ asset('images/TTM-Web-Logo-680x355.jpg') }}" class="img-fluid w-25" alt="ttm">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="competittions" role="tabpanel" aria-labelledby="competittions-tab">
                        <ul class="pl-5">
                            <li>@lang('about.competittions.paragraph.1')</li>
                            <li>@lang('about.competittions.paragraph.2')</li>
                            <li>@lang('about.competittions.paragraph.3')</li>
                            <li>@lang('about.competittions.paragraph.4')</li>
                            <li>@lang('about.competittions.paragraph.5')</li>
                            <li>@lang('about.competittions.paragraph.6')</li>
                            <li>@lang('about.competittions.paragraph.7')</li>
                        </ul>
                        <div class="row">
                            <div class="col-6 offset-3 d-flex justify-content-center">
                                <img src="{{ asset('images/faces/distintivo-m.jpeg') }}" class="img-fluid w-25" alt="Distintivo M">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="royale" role="tabpanel" aria-labelledby="royale-tab">
                        <p class="text-left">
                            @lang('about.royale.paragraph1')<br><br>
                            @lang('about.royale.paragraph2')<br><br>
                            @lang('about.royale.paragraph3')
                        </p>
                        <div class="row px-1">
                            <div class="col-12 d-flex justify-content-center">
                                <img src="{{ asset('images/groupRoyal.png') }}" class="img-fluid w-25" alt="group-Royale">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="questions" role="tabpanel" aria-labelledby="questions-tab">
                        <h4 class="text-info">@lang('about.questions.points.1.title')</h4>
                        <p class="text-left">@lang('about.questions.points.1.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.2.title')</h4>
                        <p class="text-left">@lang('about.questions.points.2.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.3.title')</h4>
                        <p class="text-left">@lang('about.questions.points.3.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.4.title')</h4>
                        <p class="text-left">@lang('about.questions.points.4.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.5.title')</h4>
                        <p class="text-left">@lang('about.questions.points.5.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.6.title')</h4>
                        <p class="text-left">@lang('about.questions.points.6.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.7.title')</h4>
                        <p class="text-left">@lang('about.questions.points.7.paragraph1')<br><br>
                            @lang('about.questions.points.7.paragraph2')</p>
                        <h4 class="text-info">@lang('about.questions.points.8.title')</h4>
                        <p class="text-left">@lang('about.questions.points.8.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.9.title')</h4>
                        <p class="text-left">@lang('about.questions.points.8.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.10.title')</h4>
                        <p class="text-left">@lang('about.questions.points.10.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.11.title')</h4>
                        <p class="text-left">
                            @lang('about.questions.points.11.paragraph1')
                            <br><br>@lang('about.questions.points.11.paragraph2')
                            <br><br>@lang('about.questions.points.11.paragraph3')
                            <br><br>@lang('about.questions.points.11.paragraph4')
                            <br><br>@lang('about.questions.points.11.paragraph5')
                            <br><br>@lang('about.questions.points.11.paragraph6')
                            <br><br>@lang('about.questions.points.11.paragraph7')
                            <br><br>@lang('about.questions.points.11.paragraph8')</p>
                        <h4 class="text-info">@lang('about.questions.points.12.title')</h4>
                        <p class="text-left">@lang('about.questions.points.12.paragraph1')</p>
                        <h4 class="text-info">@lang('about.questions.points.13.title')</h4>
                        <p class="text-left">@lang('about.questions.points.13.paragraph1')</p>
                    </div>
                    <div class="tab-pane" id="best" role="tabpanel" aria-labelledby="best-tab">
                        <h4 class="text-info">@lang('about.best.title')</h4>
                        <ul class="text-left pl-5">
                            <li>@lang('about.best.points.1.paragraph1') </li><br>
                            <ul class="pl-4">
                                <li>@lang('about.best.points.1.paragraph2')</li> <br>
                                <li>@lang('about.best.points.1.paragraph3')</li> <br>
                                <li>@lang('about.best.points.1.paragraph4')</li> <br>
                                <li>@lang('about.best.points.1.paragraph5')</li> <br>
                                <li>@lang('about.best.points.1.paragraph6')</li> <br>
                            </ul>
                            <li>@lang('about.best.points.1.paragraph7') </li><br>
                            <li>@lang('about.best.points.1.paragraph8') </li><br>
                            <li>@lang('about.best.points.1.paragraph9') </li><br>
                            <li>@lang('about.best.points.1.paragraph10')</li> <br>
                            <li>@lang('about.best.points.1.paragraph11')</li> <br>
                            <li>@lang('about.best.points.1.paragraph12')</li>
                        </ul>
                        <div class="row px-1">
                            <div class="col-12 d-flex justify-content-center">
                                <img src="{{ asset('images/pagos-con-tarjeta.png') }}" class="img-fluid w-25" alt="ttm">
                            </div>
                        </div>
                            <h4 class="text-info">@lang('about.best.points.2.title')</h4>
                            <ul class="text-left pl-5">
                                <li>@lang('about.best.points.2.paragraph1') </li><br>
                                <li>@lang('about.best.points.2.paragraph2') </li><br>
                                <ul class="pl-4">
                                    <li>@lang('about.best.points.2.paragraph3') </li><br>
                                    <li>@lang('about.best.points.2.paragraph4') </li><br>
                                    <li>@lang('about.best.points.2.paragraph5') </li><br>
                                </ul>
                                <li>@lang('about.best.points.2.paragraph6') </li><br>
                                <li>@lang('about.best.points.2.paragraph7') </li><br>
                                <li>@lang('about.best.points.2.paragraph8') </li><br>
                                <li>@lang('about.best.points.2.paragraph9') </li><br>
                                <li>@lang('about.best.points.2.paragraph10')</li> <br>
                                <li>@lang('about.best.points.2.paragraph11')</li> <br>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

