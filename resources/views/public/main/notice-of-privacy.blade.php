@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-success titles">Aviso de privacidad </h1>
                <p>Aviso dirigido a los titulares de datos personales que obran en posesión de Komextours, con domicilio en Valencia No. 9 - 301, Col. Insurgentes Mixcoac Benito Juárez, 03920
                    Ciudad de México, hace de su conocimiento que los datos personales de usted, incluyendo los sensibles, que actualmente o en el futuro obren en nuestras bases de datos, serán tratados y/o
                    utilizados por: Komextours y/o las empresas controladoras de ésta última y/o nuestras empresas filiales y/o subsidiarias y/o aquellos terceros que, por la naturaleza de sus trabajos o
                    funciones tengan la necesidad de tratar y/o utilizar sus datos personales; con el propósito de cumplir aquellas obligaciones que se derivan de la relación jurídica existente entre usted
                    como titular de los datos personales y las empresas antes señaladas.<br><br>
                    Komextours se reserva el derecho de cambiar, modificar, complementar y/o alterar el presente aviso, en cualquier momento, en cuyo caso se hará de su conocimiento a través de cualquiera
                    de los medios que establece la legislación en la materia.<br><br>
                    Aviso sobre el uso de cookies por parte de Komextours: En neycon.com se implementa el uso de "cookies" y otras tecnologías de publicidad para recopilar datos y direcciones IP; gracias
                    a ellas podemos entender mejor el comportamiento de nuestros visitantes y ofrecerles contenido personalizado, de acuerdo a los servicios y soluciones que brinda Airmatflooring.
                    Es importante señalar que, de ninguna manera, con el uso de "cookies" se extrae información de su computadora que pudiera vulnerar a nuestros clientes y visitantes. En el caso de
                    direcciones IP e información personal se sigue el protocolo vigente de uso y protección de datos manejado por KomexTours.</p>
                <h1 class="text-info titles">Aviso de confidencialidad</h1>
                <p>Rectificación: Este mensaje (y cualquier anexo) es privado y confidencial y para el destinatario exclusivamente. El acceso, la distribución, el copiado o referencias a su contenido por
                    cualquier persona, está prohibido por las leyes aplicables. Si usted recibe este mensaje por error, por favor infórmenos de inmediato por teléfono y elimine el mensaje y cualquier copia
                    de su sistema. Opiniones expresadas en esta comunicación no necesariamente corresponden a las del Grupo ROYALE. Cada receptor de correo es obligado de verificar cada correo por virus y
                    por tal motivo no nos hacemos responsable por infecciones traspasados – Gracias.</p>
                <p><strong>Disclaimer:</strong> This email (and any attachment) is private and confidential and is for the intended recipient only. Access, disclosure, copying, distribution or reliance
                    on any of it by anyone else is prohibited and may be a criminal offence. If obtained in error please notify the sender and delete it and all copies. The opinions expressed within this
                    communication are not necessarily those expressed by ROYALE Group. Each recipient of mail s is obliged to check each mail for virus and due to this a liability for forwarded infections
                    is excluded. – Thank you.</p>
            </div>
        </div>
    </div>
@endsection
