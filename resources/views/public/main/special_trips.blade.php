@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-capitalize text-success font-weight-bold titles">@lang('extensions.special_trips.title')</h1>

                <div class="row">
                    <div class="col-12">
                        <form action="" class="group-form py-4">
                            <input type="text" class="form-control mb-3" placeholder="{{ trans('extensions.search') }}" id="search" onkeyup="change(this.value)">
                            <select name="sector" id="sector" class="form-control" onchange="change(this.value)">
                                <option value="">@lang('extensions.special_trips.select')</option>
                                @foreach($regions as $region)
                                    <option value="{{$region->id}}">{{$region->region}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
                <h4 class="text-capitalize title-card font-weight-bold subtitles">@lang('extensions.special_trips.slogan')</h4>
                <hr>
                <div class="row" id="result">
                    <div class="col-12 mb-5">
                        {{ $extensions->links() }}

                        @foreach($extensions as $extension)
                            <div class="row border-bottom border-success">
                                <div class="col-12 col-lg-3 py-3">
                                    @php($image = DB::table('images')->where('type', 'special_trips')->where('reference', $extension->id)->first())
                                    <a href="{{ route('specialTrip', ['slug' => $extension->slug]) }}">
                                        @if($image)
                                            <img src="{{asset($image->name)}}" class="img-fluid w-100 image" alt="{{$image->name}}" id="logo">
                                        @else
                                            <img src="{{asset('images/missing.png')}}" class="img-fluid w-100 image" alt="Sin imagen" id="logo">
                                        @endif
                                    </a>
                                </div>
                                <div class="col-12 col-lg-9 py-3">
                                    <h4 class="title-card titles">{{ $extension->name }}</h4>
                                    <p>
                                        <span class="title-card">@lang('extensions.region'):</span>
                                        @if($extension->region)<span>{{$extension->region->region}}</span> @endif <br>
                                        <span class="title-card">@lang('extensions.destination'):</span>
                                        @if($extension->destino)<span>{{$extension->destino->destination}}</span>@endif <br>
                                        <span class="title-card">@lang('extensions.category'):</span>
                                        @if($extension->category)<span>{{$extension->category->category}}</span>@endif
                                    </p>
                                    <p class="text-left">
                                        {!! Str::words($extension->description , 40, ' ...') !!}
                                    </p>
                                    <a href="{{ route('specialTrip', ['slug' => $extension->slug]) }}" class="btn btn-info align-right">@lang('fairs.more')</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $extensions->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        function change(param){
            var result = document.getElementById('result');
            axios.post('/viajes_especiales/component', {
                param: param,
            }).then(function (response) {
                // console.log(response);
                result.innerHTML = response.data;
            }).catch(function (error){
                console.log(error);
            });
        }
    </script>
@endsection
