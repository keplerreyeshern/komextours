@extends('public.layouts.app')

@section('head')
    <meta property="og:url"           content="{{ config('app.url') }}/ofertas/{{$offer->slug}}" />
    <meta property="og:type"          content="Oferta" />
    <meta property="og:title"         content="{{$offer->name}}" />
    <meta property="og:description"   content="{!! $offer->description !!}" />
    <meta property="og:image"         content="{{ config('app.url') }}/{{$image->name}}" />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-success titles">{{$offer->name}}</h1>
                @if($image)
                    <img src="{{asset($image->name)}}" class="img-fluid w-25 image" alt="{{$image->name}}" id="logo">
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-25 image" alt="Sin imagen" id="poster">
                @endif
                <p class="mt-3">
                    <span class="text-info font-bold">@lang('extensions.show.region')</span>
                    {{ $offer->region->region }}
                </p>
                <p>
                    <span class="text-info font-bold">@lang('extensions.show.date')</span>
                    <span class="tex-capitalize">
                        {{ \Carbon\Carbon::parse($offer->startDate)->translatedFormat('d F Y') }} a
                        {{ \Carbon\Carbon::parse($offer->endDate)->translatedFormat('d F Y') }}
                    </span>
                </p>
                <p class="font-bold">
                    <span class="text-info" >@lang('extensions.show.category')</span>
                    <span>{{ $offer->category->category }}</span>
                </p>
                <div class="container mt-3">
                    <div class="row d-flex justify-content-center">
                        @foreach($images as $keyg => $gallery)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <img src="{{asset($gallery->name)}}" class="img-fluid image" alt="{{$gallery->name}}" id="Imagen de galeria {{ $keyg + 1 }}">
                            </div>
                        @endforeach
                    </div>
                </div>
{{--                <p class="text-info font-bold mt-3">@lang('extensions.show.leagues')</p>--}}
{{--                <ul class="border">--}}
{{--                    @forelse($offer->leagues as $key => $league)--}}
{{--                        <li>--}}
{{--                            <a href="{{ $league->url }}" class="nav-link link-league">--}}
{{--                                <p>--}}
{{--                                    {{ $league->name }}--}}
{{--                                </p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    @empty--}}
{{--                        <p>No tiene ninguna liga asociada</p>--}}
{{--                    @endforelse--}}
{{--                </ul>--}}
                <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fbeta.komextours.com%2Fofertas%2F{{$offer->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=220604468023332" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                <iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fbeta.komextours.com%2Fofertas%2F{{$offer->slug}}%2F&layout=button_count&size=small&appId=200796963753636&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
            </div>
        </div>
    </div>
@endsection
