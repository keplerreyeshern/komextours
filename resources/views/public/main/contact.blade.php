@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="text-info font-weight-bold titles">@lang('contact.slogan')</h4>
                <h1 class="text-capitalize text-success font-weight-bold titles">@lang('contact.title')</h1>
                <div class="row align-items-center">
                    <div class="col-2">
                        <img src="{{ asset('images/telephone.png') }}" class="img-fluid w-100" alt="Telephone">
                    </div>
                    <div class="col-10">
                        <p class="h6 text-dark">@lang('contact.telephoneswitch') <span class="text-info">+52 55 5615 8741</span></p>
{{--                        <p class="h6 text-dark">@lang('contact.admin') <span class="text-info">Ext. 2</span></p>--}}
                        <p class="h6 text-dark">@lang('contact.lada') <span class="text-info">800 849 1514</span></p>
                        <p class="h6 text-dark">Skype:</p>
                        <p class="h6 text-dark">&nbsp;&nbsp;&nbsp;&nbsp;*@lang('contact.fairs') <span class="text-info">SuzanneHolst1</span></p>
                        <p class="h6 text-dark">&nbsp;&nbsp;&nbsp;&nbsp;*@lang('contact.incoming') <span class="text-info">hpholst1</span></p>
                        <p class="h6 text-dark">@lang('contact.whatsapp')</p>
                        <p class="h6 text-dark">&nbsp;&nbsp;&nbsp;&nbsp;*@lang('contact.fairs') <span class="text-info">+52 55 5407 4786</span></p>
                        <p class="h6 text-dark">&nbsp;&nbsp;&nbsp;&nbsp;*@lang('contact.incoming') <span class="text-info">+52 55 5453 0398</span></p>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-2">
                        <img src="{{ asset('images/email.png') }}" class="img-fluid w-100" alt="Email">
                    </div>
                    <div class="col-10">
                        <p class="h6 text-dark">@lang('contact.email') <span class="text-info">info@komextours.com</span></p>
                    </div>
                </div>
                <form action="" class="form-group">
                    <div class="row">
                        <div class="col-12 col-md-3 text-right">
                            <h2 class="text-info">@lang('contact.form.name')</h2>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3 text-right">
                            <h2 class="text-info">@lang('contact.form.telephone')</h2>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="telephone" class="form-control" name="telephone">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3 text-right">
                            <h2 class="text-info">@lang('contact.form.email')</h2>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="email" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3 text-right">
                            <h2 class="text-info">@lang('contact.form.message')</h2>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea type="text" class="form-control" name="message"></textarea>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <button type="submit" class="btn btn-info align-right">Enviar</button>
                        </div>
                    </div>
                </form>
                <h1 class="text-success font-weight-bold titles">@lang('contact.mailboxes')</h1>
                <hr class="hr">
                <div class="row align-item-center">
                    <div class="col-3 text-right align-item-center">
                        <p class="h6 text-dark text-right">@lang('contact.general')</p>
                    </div>
                    <div class="col-9 text-left align-item-center">
                        <p class="h6 text-info text-left">komex@royaletours.com.mx</p>
                        <p class="h6 text-info text-left">info@komextours.com</p>
                    </div>
                </div>
                <div class="row align-item-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.fairsandextensions')</p>
                    </div>
                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">ferias@komextours.com</p>
                    </div>
                </div>
                <div class="row align-item-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.specialtrips')</p>
                    </div>
                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">gerencia.ferias@royaletours.com.mx</p>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.passion')</p>
                    </div>

                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">incoming@komextours.com</p>
                    </div>
                </div>
                <div class="row align-item-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.destination')</p>
                    </div>
                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">incoming.groups@royaletours.com.mx</p>
                    </div>
                </div>
                <div class="row align-item-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.conven')</p>
                    </div>
                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">management@royaletours.com.mx</p>
                    </div>
                </div>
                <div class="row align-item-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.admin')</p>
                    </div>
                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">administracion1@komextours.com</p>
                    </div>
                </div>
                <div class="row align-item-center">
                    <div class="col-3 text-right align-items-center">
                        <p class="h6 text-dark text-right">@lang('contact.locate')</p>
                    </div>
                    <div class="col-9 text-left align-items-center">
                        <p class="h6 text-info text-left">management@komextours.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
