@if(isset($page))
<div class="container-fluid banner
@switch($page)
    @case('about')
        -bg-about
    @break
    @case('services')
        -bg-services
    @break
    @case('events')
        -bg-fairs
    @break
    @case('extensions')
        -bg-extensions
    @break
    @case('mexico')
        -bg-mexico
    @break
    @case('special_trips')
        -bg-special_trips
    @break
    @case('contact')
        -bg-contact
    @break
    @case('testimonials')
        -bg-testimonials
    @break
    @default
        -bg-contact
    @break
@endswitch
">
</div>
@endif


