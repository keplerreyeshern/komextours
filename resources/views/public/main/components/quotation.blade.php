<a href="#miModal" class="btn btn-primary">
    @lang('quotation.request')
</a>
<div id="miModal" class="-modal" style="overflow-y: scroll;">
    <div class="-modal-contenido" style="margin: {{$page == 'fairs' ? '3%': '5%'}} auto;">
        <a href="#" class="align-right">X</a>
        <div class="p-3">
            <h2 class="text-center">@lang('quotation.request')</h2>
            <form action="{{ route('quotation') }}" method="post">
                @csrf
                <div class="row mt-2">
                    <label for="">@lang('quotation.name')</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="row mt-2">
                    <label for="">@lang('quotation.telephone')</label>
                    <input type="number" min="0" class="form-control" name="telephone" required>
                </div>
                <div class="row mt-2">
                    <label for="">@lang('quotation.email')</label>
                    <input type="email" class="form-control" name="email" required>
                </div>
                @if($page == 'fairs')
                    <div class="row mt-2">
                        <label for="">@lang('quotation.company')</label>
                        <input type="text" class="form-control" name="company" required>
                    </div>
                    <div class="row mt-2">
                        <label for="">@lang('quotation.visit')</label>
                        <input type="text" class="form-control" name="visit" required>
                    </div>
                    <div class="row mt-2">
                        <label for="">@lang('quotation.days') </label>
                        <input type="number" min="1" class="form-control" name="days" required>
                    </div>
                @endif
                <div class="row mt-2">
                    <label for="">@lang('quotation.date')</label>
                    <input type="date" class="form-control" name="date" onkeydown="return false" required>
                </div>
                <div class="row mt-2">
                    <label for="">@lang('quotation.duration')</label>
                    <input type="number" min="1" class="form-control" name="duration" required>
                </div>
                <div class="row mt-2">
                    <label for="">@lang('quotation.location')</label>
                    <input type="text" class="form-control" name="location" required>
                </div>
                <div class="row mt-2">
                    <label for="">@lang('quotation.persons')</label>
                    <input type="number" min="1" class="form-control" name="no_persons" required>
                </div>
                <div class="row mt-2">
                    <label for="notes">@lang('quotation.notes')</label>
                    <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <input type="hidden" name="type" value="{{$page}}">
                <input type="hidden" name="reference" value="{{$reference}}">
                <div class="row mt-2">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary align-right">
                            @lang('quotation.request')
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(window).ready(function (){
        // alert('mensaje');
    });
</script>
