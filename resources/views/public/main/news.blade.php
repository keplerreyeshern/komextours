@extends('public.layouts.app')

@section('content')
    <div class="container-fluid content-komex">
        <div class="row">
            <div class="col-12">
                @if($page == 'blog')
                    <h1 class="text-capitalize text-success font-weight-bold titles">@lang('news.blog.title')</h1>
                @elseif($page == 'news')
                    <h1 class="text-capitalize text-success font-weight-bold titles">@lang('news.news.title')</h1>
                @endif
                <div class="row mb-5" id="result">
                    <div class="col-12">
                        {{ $news->links() }}

                        @foreach($news as $new)
                            <div class="row border-bottom border-success">
                                <div class="col-12 col-lg-3 py-3">
                                    @if($page == 'blog')
                                        <a href="{{ route('blog', ['slug' => $new->slug] ) }}">
                                    @elseif($page == 'news')
                                        <a href="{{ route('newsShow', ['slug' => $new->slug] ) }}">
                                    @endif
                                        @if($new->image)
                                            <img src="{{asset($new->image)}}" class="img-fluid w-100 image" alt="{{$new->image}}" id="logo">
                                        @else
                                            <img src="{{asset('images/missing.png')}}" class="img-fluid w-100 image" alt="Sin imagen" id="logo">
                                        @endif
                                        </a>
                                </div>
                                <div class="col-12 col-lg-9 py-3">
                                    <h4 class="text-info">{{ $new->name }}</h4>
                                    <p class="text-left img-fluid w-100 image">{!! $new->intro !!}</p>
                                    @if($page == 'blog')
                                        <a href="{{ route('blog', ['slug' => $new->slug] ) }}" class="btn btn-info align-right">@lang('news.more')</a>
                                    @elseif($page == 'news')
                                        <a href="{{ route('newsShow', ['slug' => $new->slug] ) }}" class="btn btn-info align-right">@lang('news.more')</a>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
