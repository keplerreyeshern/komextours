@extends('public.layouts.app')

@section('head')
    @if($page == 'blog' )
        <meta property="og:url"           content="{{ config('app.url') }}/blog/{{$new->slug}}" />
        <meta property="og:type"          content="Blog" />
    @elseif($page == 'news' )
        <meta property="og:url"           content="{{ config('app.url') }}/noticias/{{$new->slug}}" />
        <meta property="og:type"          content="Noticia" />
    @endif
    <meta property="og:title"         content="{{$new->name}}" />
    <meta property="og:description"   content="{{ $new->content }}" />
    <meta property="og:image"         content="{{ config('app.url') }}/{{$new->image}}" />
@endsection

@section('content')
    <div id="fb-root"></div>
    @if(App::islocale('es'))
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v9.0&appId=463005901528543&autoLogAppEvents=1" nonce="ykb4vpSp"></script>
    @endif
    @if(App::islocale('en'))
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0&appId=463005901528543&autoLogAppEvents=1" nonce="ykb4vpSp"></script>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-12 mt-2">
                <h1 class="text-success titles">{{$new->title}}</h1>
                @if($new->image)
                    <img src="{{asset($new->image)}}" class="img-fluid w-25 image" alt="{{$new->image}}" id="logo">
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-25 image" alt="Sin imagen" id="poster">
                @endif
                <h2>
                    <span class="text-info font-bold">@lang('news.show.date')</span>
                    <span class="tex-capitalize">
                        {{ \Carbon\Carbon::parse($new->date)->translatedFormat('d F Y') }}
                    </span>
                </h2>
                <h2>{!! $new->content !!}</h2>

                @if($page == 'blog' )
                    @if(App::islocale('es'))
                        <iframe src="https://www.facebook.com/plugins/like.php?locale=es_LA&href={{ config('app.url') }}%2Fblog%2F{{$new->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                        <iframe src="https://www.facebook.com/plugins/share_button.php?locale=es_LA&href={{ config('app.url') }}%2Fblog%2F{{$new->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    @endif
                    @if(App::islocale('en'))
                            <iframe src="https://www.facebook.com/plugins/like.php?locale=en_US&href={{ config('app.url') }}%2Fblog%2F{{$new->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                            <iframe src="https://www.facebook.com/plugins/share_button.php?locale=en_US&href={{ config('app.url') }}%2Fblog%2F{{$new->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    @endif
                @elseif($page == 'news' )
                    @if(App::islocale('es'))
                        <iframe src="https://www.facebook.com/plugins/like.php?locale=es_LA&href={{ config('app.url') }}%2Fnoticias%2F{{$new->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                        <iframe src="https://www.facebook.com/plugins/share_button.php?locale=es_LA&href={{ config('app.url') }}%2Fnoticias%2F{{$new->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    @endif
                    @if(App::islocale('en'))
                            <iframe src="https://www.facebook.com/plugins/like.php?locale=en_US&href={{ config('app.url') }}%news%2F{{$new->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                            <iframe src="https://www.facebook.com/plugins/share_button.php?locale=en_US&href={{ config('app.url') }}%news%2F{{$new->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    @endif
                @endif
            </div>
        </div>
    </div>

@endsection

