@extends('administrador.layouts.app-admin')

@section('styles')
    <style type="text/css">
        #map {
            width:100%;
            height: 500px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Editar feria {{$fair->name}}</h1>
                        <div class="col-2">
                            <a href="{{route('fairs.index')}}" class="btn btn-primary">
                                <i class="fa fa-list"></i>
                                Lista
                            </a>
                        </div>
                    </div>
                </div>
                <form class="validate-form mb-5" method="POST" action="{{route('fairs.update', ['fair' => $fair->id])}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                        <i class="fa fa-flag text-info"></i>
                                @endif
                                    Nombre
                            </label>
                            <input type="text" class="form-control mt-3 @error('name') is-invalid @enderror" placeholder="Nombre" name="name" value="{{old('name', $fair->name)}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="">URL</label>
                            <input type="text" class="form-control mt-3" placeholder="Url" name="url" value="{{old('url', $fair->url)}}">
                            @error('url')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="">Fecha de Inicio</label>
                            <input type="date" class="form-control mt-3 @error('startDate') is-invalid @enderror" value="{{old('startDate', $fair->startDate)}}"
                                   onkeydown="return false"
                                   placeholder="Fecha Inicio"
                                   required name="startDate">
                            @error('startDate')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="">Fecha de fin</label>
                            <input type="date" class="form-control mt-3 @error('endDate') is-invalid @enderror" value="{{old('endDate', $fair->endDate)}}"
                                   onkeydown="return false"
                                   placeholder="Fecha Fin"
                                   required name="endDate">
                            @error('endDate')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="">Ramo</label>
                            <select name="sector" id=""
                                    class="form-control @error('sector') is-invalid @enderror">
                                @foreach($sectors as $sector)
                                    <option value="{{$sector->id}}" {{$sector->id == $fair->sector_id ? 'selected':''}}>{{$sector->name}}</option>
                                @endforeach
                            </select>
                            @error('sector')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Lugar
                            </label>
                            <input type="text" class="form-control @error('place') is-invalid @enderror" placeholder="Lugar" name="place" value="{{old('place', $fair->place)}}">
                            @error('place')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Descripción Breve
                            </label>
                            <textarea class="ckeditor mt-3 @error('short_description') is-invalid @enderror" placeholder="Descripción corta"
                                      name="short_description" value="{{old('short_description', $fair->short_description)}}">
                                {{old('short_description', $fair->short_description)}}
                            </textarea>
                            @error('short_description')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Descripción
                            </label>
                            <textarea class="ckeditor mt-3 @error('description') is-invalid @enderror" placeholder="Descripción"
                                      name="description" value="{{old('description', $fair->description)}}">
                                {{old('description', $fair->description)}}
                            </textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Servicios
                            </label>
                            <textarea class="ckeditor mt-3 @error('services') is-invalid @enderror bg-info" placeholder="Servicios"
                                      name="services" value="{{old('services', $fair->services)}}">
                                {{old('services', $fair->services)}}
                            </textarea>
                            @error('services')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <div class="row">
                        <div class="col-6">
                            <label for="">Video</label>
                            <input type="text" class="form-control mt-3" placeholder="Video"  name="video" value="{{old('video', $fair->video)}}">
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Poster</label>
                            <br>
                            @if($fair->poster)
                                <img src="{{asset($fair->poster)}}" class="img-fluid" alt="{{$fair->poster}}" id="poster">
                            @else
                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="poster">
                            @endif
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Poster (Formato png, jpg o jpeg 320 x 640 resolución 72 dpi, max 900Kb )<br>
                            </div>

                            <div class="upload-btn-wrapper" id="btnposter">
                                <button class="btn btn-primary">Selecciona un Archivo</button>
                                <input type="file" name="poster" accept="image/*" onchange="loadFilePoster(event)">
                            </div>
                            <div id="deleteposter">
                                <a class="btn btn-danger" onclick="deletePoster()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Logo</label>
                            <br>
                            @if($fair->logo)
                                <img src="{{asset($fair->logo)}}" class="img-fluid" alt="{{$fair->logo}}" id="logo">
                            @else
                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="logo">
                            @endif
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Logo (Formato png, jpg o jpeg 320 x 320 resolución 72 dpi, max 900Kb )<br>
                            </div>
                            <div class="upload-btn-wrapper" id="btnlogo">
                                <button class="btn btn-primary">Selecciona un Archivo</button>
                                <input type="file" name="logo" accept="image/*" onchange="loadFileLogo(event)">
                            </div>
                            <div id="deletelogo">
                                <a class="btn btn-danger" onclick="deleteLogo()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="alert alert-info col-12" role="alert">
                                <i class="fa fa-file-image"></i>
                                Fotos (formato png, jpg o jpeg 1024 x 798 resolución 72 dpi, max 900Kb)
                            </div>
                            <div class="row mb-5">
                                @if (session('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <strong>{{ session('success') }}</strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @foreach($images as $image)
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                        <img src="{{asset($image->name)}}" class="img-fluid image" alt="{{$image->name}}" id="logo">
                                        <a href="{{route('images.destroy', ['id' => $image->id])}}" class="btn text-danger">
                                            <i class="fa fa-trash-alt"></i>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                            <dropzone-component :reference="{!!$fair->id!!}" type="fairs"></dropzone-component>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-6">
                            <input type="text" placeholder="Latutide" class="form-control" name="latitude" id="latitude" value="">
                            @error('latitude')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <input type="text" placeholder="Longitude" class="form-control" name="longitude" id="longitude" value="">
                            @error('longitude')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                Ubicación
                            </label>
                            <input type="text" class="form-control mt-3"
                                   placeholder="Ubicación" id="location"
                                   required
                                   name="location" value="">
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12" id="map"></div>
                    </div>
                    <div class="row mt-5 mb-5">
                        <div class="col-12">
                            <label for="leagues">Ligas</label>
                            <div class="border rounded p-3" id="leagues">
                                @foreach($leagues as $league)
                                    <div class="form-check">
                                        <input class="checkbox"
                                               value="{{$league->id}}"
                                               type="checkbox"
                                               name="leagues[]"
                                               @foreach($fair->leagues as $leagueFair)
                                                    @if($league->id == $leagueFair->id)
                                                         checked
                                                    @endif
                                               @endforeach
                                        >
                                        <label class="form-check-label">
                                            {{ $league->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <input type="checkbox" class="checkbox" name="active" id="active" checked>
                            <label for="active">Activo</label>
                        </div>
                    </div>
                    <div class="row mt-3 mb-5">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-edit"></i>
                                Actualizar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @php
        $location = $fair->location;
        $pos1 = strpos($location, ',');
        $pos1 = $pos1 + 1;
        $rest = substr($location, $pos1);
        $pos2 = strpos($rest, ',');
        $address = substr($rest, 0 ,$pos2);
    @endphp
    <input type="hidden" id="hidden_latitude" value="{{$fair->latitude}}">
    <input type="hidden" id="hidden_longitude" value="{{$fair->longitude}}">
    <input type="hidden" id="hidden_address" value="{{$address}}">
@endsection
@section('scripts')
    <script type="text/javascript">
        document.getElementById('deleteposter').style.display = 'none';
        document.getElementById('deletelogo').style.display = 'none';
        function deletePoster(){
            document.getElementById('deleteposter').style.display = 'none';
            document.getElementById('btnposter').style.display = 'block';
            var image = document.getElementById('poster');
            image.src = '/images/missing.png';
            image.value = "";
        }
        function deleteLogo(){
            document.getElementById('deletelogo').style.display = 'none';
            document.getElementById('btnlogo').style.display = 'block';
            var image = document.getElementById('logo');
            image.src = '/images/missing.png';
            image.value = "";
        }
        var loadFilePoster = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('poster');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btnposter').style.display = 'none';
            document.getElementById('deleteposter').style.display = 'block';
        };
        var loadFileLogo = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('logo');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btnlogo').style.display = 'none';
            document.getElementById('deletelogo').style.display = 'block';
        };
        var map;
        var geocoder;
        var latitude = parseFloat(document.getElementById("hidden_latitude").value).toFixed(14);
        var longitude = parseFloat(document.getElementById("hidden_longitude").value).toFixed(14);
        var address = document.getElementById("hidden_address").value;
        // window.addEventListener("load", loadingPage);
        // function loadingPage(){
        //     var text_latitude = document.getElementById("latitude");
        //     text_latitude.value = latitude;
        //     var text_longitude = document.getElementById("longitude");
        //     text_longitude.value = longitude;
        // }

        function updateMarkerPosition(latLng) {
            var text_latitude = document.getElementById("latitude");
            var text_longitude = document.getElementById("longitude");
            text_latitude.value = latLng.lat();
            text_longitude.value = latLng.lng();
        }

        function updateMarkerAddress(str) {
            //document.getElementById('address').innerHTML = str;
            var text_address = document.getElementById('location');
            text_address.value = str;
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function(responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('No se puede determinar la dirección en esta ubicación.');
                }
            });
        }

        function initMap() {
            var mapLayer = document.getElementById("map");
            var centerCoordinates = new google.maps.LatLng(latitude, longitude);
            var defaultOptions = { center: centerCoordinates, zoom: 8 }

            map = new google.maps.Map(mapLayer, defaultOptions);
            geocoder = new google.maps.Geocoder();

            geocoder.geocode( { 'address': address }, function(LocationResult, status) {
                // if (status == google.maps.GeocoderStatus.OK) {
                //     var latitude = LocationResult[0].geometry.location.lat();
                //     var longitude = LocationResult[0].geometry.location.lng();
                // }
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: 'KomexTours',
                    draggable: true,
                    clickable: true,
                    animation: google.maps.Animation.DROP
                });
                // Update current position info.
                updateMarkerPosition(marker.getPosition());
                geocodePosition(marker.getPosition());

                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function() {
                    updateMarkerAddress('Dragging...');
                });

                google.maps.event.addListener(marker, 'drag', function() {
                    updateMarkerPosition(marker.getPosition());
                });

                google.maps.event.addListener(marker, 'dragend', function() {
                    geocodePosition(marker.getPosition());
                });
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8_8kSrBymZR_OLHLeCfWZFrX7l1HNUTE&callback=initMap"></script>
@endsection
