@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{route('fairs.create')}}" class="-btn-agregar">
                    <i class="fas fa-user-plus"></i>
                    Agregar
                </a>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Ferias
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover mt-3">
                            <thead>
                            <tr>
                                <th width="200px">Imagen</th>
                                <th>Nombre</th>
                                <th>Ramo</th>
                                <th>Lugar</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fecha</th>
                                <th colspan="3">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($fairs->sortby('name') as $fair)
                                <tr>
                                    <td width="200px">
                                        <a href="{{route('fairs.edit', ['fair' => $fair->id])}}">
                                            @if($fair->logo)
                                                <img src="{{asset($fair->logo)}}" class="img-fluid" alt="{{$fair->logo}}" id="logo">
                                            @else
                                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="logo">
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{route('fairs.edit', ['fair' => $fair->id])}}">
                                            {{$fair->name}}
                                        </a>
                                    </td>
                                    <td>{{$fair->sector->name}}</td>
                                    <td>{{$fair->place}}</td>
                                    <td>{{$fair->startDate}}</td>
                                    <td>{{$fair->endDate}}</td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('fairs.active', ['id' => $fair->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($fair->active)
                                                    <i class="fa fa-power-off -on"></i>
                                                @else
                                                    <i class="fa fa-power-off -off"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('fairs.star', ['id' => $fair->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn text-info">
                                                @if($fair->star)
                                                    <i class="fas fa-star"></i>
                                                @else
                                                    <i class="far fa-star"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('fairs.destroy', ['fair' => $fair->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
