@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Editar Usuario {{$user->name}}</h1>
                        <div class="col-2">
                            <a href="{{route('users.index')}}" class="btn btn-primary">
                                <i class="fa fa-list"></i>
                                Lista
                            </a>
                        </div>
                    </div>
                </div>
                <form class="validate-form mb-5" method="POST" action="{{route('users.update', ['user' => $user->id])}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-12">
                            <label for="">
                                Nombre
                            </label>
                            <input type="text" class="form-control mt-3 @error('name') is-invalid @enderror" placeholder="Nombre" name="name" value="{{old('name', $user->name)}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="">
                                Correo
                            </label>
                            <input type="email" class="form-control mt-3 @error('email') is-invalid @enderror" placeholder="Correo" name="email" value="{{old('email', $user->email)}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="">
                                Contraseña
                            </label>
                            <input id="password" placeholder="Contraseña" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="">
                                Confirma la contraseña
                            </label>
                            <input id="password-confirm" placeholder="Confirma la contraseña" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
