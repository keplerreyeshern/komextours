@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                @if($title == 'Extensiones')
                    <a href="{{route('extensions.create')}}" class="-btn-agregar">
                @elseif($title == 'México')
                    <a href="{{route('mexico.create')}}" class="-btn-agregar">
                @elseif($title == 'Ofertas')
                    <a href="{{route('offers.create')}}" class="-btn-agregar">
                @else
                    <a href="{{route('trips.create')}}" class="-btn-agregar">
                @endif
                    <i class="fas fa-user-plus"></i>
                    Agregar
                </a>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        {{$title}}
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover mt-3">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                @if($title != 'Ofertas')
                                    <th>Código</th>
                                @endif
                                <th>Región</th>
                                <th>Categoria</th>
                                <th width="200px">Fecha Inicio/Fin</th>
                                <th colspan="2" width="50px">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($extensions->sortby('name') as $extension)
                                <tr>
                                    <td>
                                        @if($title == 'Extensiones')
                                            <a href="{{route('extensions.edit', ['extension' => $extension->id])}}">
                                        @elseif($title == 'México')
                                            <a href="{{route('mexico.edit', ['mexico' => $extension->id])}}">
                                        @elseif($title == 'Ofertas')
                                            <a href="{{route('offers.edit', ['offer' => $extension->id])}}">
                                        @else
                                            <a href="{{route('trips.edit', ['trip' => $extension->id])}}">
                                        @endif
                                            {{$extension->name == '' ? 'edit':$extension->name}}
                                        </a>
                                    </td>
                                    @if($title != 'Ofertas')
                                        <td>{{$extension->code}}</td>
                                    @endif
                                    <td>{{$extension->region->region}}</td>
                                    <td>{{$extension->category->category}}</td>
                                    <td>
                                        {{$extension->startDate}}<br>
                                        {{$extension->endDate}}
                                    </td>
                                    <td width="10px">
                                        @if($title == 'Extensiones')
                                            <form method="POST" action="{{ route('extensions.active', ['id' => $extension->id]) }}" class="form-check-inline">
                                        @elseif($title == 'México')
                                            <form method="POST" action="{{ route('mexico.active', ['id' => $extension->id]) }}" class="form-check-inline">
                                        @elseif($title == 'Ofertas')
                                            <form method="POST" action="{{ route('offers.active', ['id' => $extension->id]) }}" class="form-check-inline">
                                        @else
                                            <form method="POST" action="{{ route('trips.active', ['id' => $extension->id]) }}" class="form-check-inline">
                                        @endif
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($extension->active)
                                                    <i class="fa fa-power-off -on"></i>
                                                @else
                                                    <i class="fa fa-power-off -off"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        @if($title == 'Extensiones')
                                            <form method="POST" action="{{ route('extensions.destroy', ['extension' => $extension->id]) }}" class="form-check-inline">
                                        @elseif($title == 'México')
                                            <form method="POST" action="{{ route('mexico.destroy', ['mexico' => $extension->id]) }}" class="form-check-inline">
                                        @else
                                            <form method="POST" action="{{ route('trips.destroy', ['trip' => $extension->id]) }}" class="form-check-inline">
                                        @endif
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
