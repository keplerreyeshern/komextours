@extends('administrador.layouts.app-admin')

@section('styles')
    <style type="text/css">
        #map {
            width:100%;
            height: 500px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Crear {{$single}}</h1>
                        <div class="col-2">
                            @if($title == 'Extensiones')
                                <a href="{{route('extensions.index')}}" class="btn btn-primary">
                            @elseif($title == 'México')
                                <a href="{{route('mexico.index')}}" class="btn btn-primary">
                            @elseif($title == 'Viajes Especiales')
                                <a href="{{route('trips.index')}}" class="btn btn-primary">
                            @elseif($title == 'Ofertas')
                                <a href="{{route('offers.index')}}" class="btn btn-primary">
                            @endif
                                <i class="fa fa-list"></i>
                                    Lista
                                </a>
                        </div>
                    </div>
                </div>
                @if($title == 'Extensiones')
                    <form class="validate-form mb-5" method="POST" action="{{route('extensions.store')}}" enctype="multipart/form-data">
                @elseif($title == 'México')
                    <form class="validate-form mb-5" method="POST" action="{{route('mexico.store')}}" enctype="multipart/form-data">
                @elseif($title == 'Viajes Especiales')
                    <form class="validate-form mb-5" method="POST" action="{{route('trips.store')}}" enctype="multipart/form-data">
                @elseif($title == 'Ofertas')
                    <form class="validate-form mb-5" method="POST" action="{{route('offers.store')}}" enctype="multipart/form-data">
                @endif
                    @csrf
                    <div class="row">
                        @if($title != 'Ofertas')
                            <div class="col-12 col-md-6">
                        @else
                            <div class="col-12">
                        @endif
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Nombre
                            </label>
                            <input type="text" class="form-control mb-1 @error('name') is-invalid @enderror" placeholder="Nombre" name="name" value="{{old('name')}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        @if($title != 'Ofertas')
                            <div class="col-12 col-md-6">
                                <label for="code">Codigo</label>
                                <input type="text" class="form-control mb-1 @error('code') is-invalid @enderror" placeholder="Codigo" id="code" name="code" value="{{old('code')}}">
                                @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="startDate">Fecha de Inicio</label>
                            <input type="date" class="form-control mb-1 @error('startDate') is-invalid @enderror" id="startDate"
                                   onkeydown="return false"
                                   placeholder="Fecha Inicio"
                                   required name="startDate" value="{{old('startDate')}}">
                            @error('startDate')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="endDate">Fecha de fin</label>
                            <input type="date" class="form-control mb-1 @error('endDate') is-invalid @enderror" id="endDate"
                                   onkeydown="return false"
                                   placeholder="Fecha Fin"
                                   required name="endDate" value="{{old('endDate')}}">
                            @error('endDate')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="region">Region</label>
                            <select name="region" id="region"
                                    class="form-control @error('region') is-invalid @enderror"
                                    required >
                                @foreach($regions as $region)
                                    <option value="{{$region->id}}">{{$region->region}}</option>
                                @endforeach
                            </select>
                            @error('region')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            @if($title == 'Ofertas')
                                <label for="fair">Feria</label>
                                <select name="fair" id="fair"
                                        class="form-control  @error('fair') is-invalid @enderror">
                                    @foreach($fairs as $fair)
                                        <option value="{{$fair->id}}">{{$fair->name}}</option>
                                    @endforeach
                                </select>
                                @error('fair')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            @else
                                <label for="destination_id">Destino</label>
                                <select name="destination_id" id="destination_id"
                                        class="form-control  @error('destination_id') is-invalid @enderror">
                                    @if($title == 'México')
                                        @foreach($destinationsMexico as $destination)
                                            <option value="{{$destination->id}}" >{{$destination->region}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @error('destination_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            @endif
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="itinerary_file">Itinerario</label><br>
                            <div class="upload-btn-wrapper" id="itinerary_file">
                                <button class="btn btn-primary">Selecciona un archivo</button>
                                <input type="file" name="itinerary_file">
                            </div>
                            <div id="namefile"></div>
                            @error('itinerary_file')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="category">Categoria</label>
                            <select name="category" id="category"
                                    class="form-control  @error('category') is-invalid @enderror">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                            </select>
                            @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="text" class="form-control mt-3 @error('hotel') is-invalid @enderror" placeholder="Hotel" required name="hotel" value="{{old('hotel')}}">
                            @error('hotel')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Facilidades
                            </label>
                            <textarea class="ckeditor mt-3 @error('facilities') is-invalid @enderror" placeholder="Facilidades"
                                      name="facilities" value="{{old('facilities')}}">
                                {{old('facilities')}}
                            </textarea>
                            @error('facilities')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Descripción
                            </label>
                            <textarea class="ckeditor mt-3 @error('description') is-invalid @enderror" placeholder="Descripción"
                                      name="description" value="{{old('description')}}">
                                {{old('description')}}
                            </textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Tarifas
                            </label>
                            <textarea class="ckeditor mt-3 @error('rates') is-invalid @enderror" placeholder="Tarifas"
                                      name="rates" value="{{old('rates')}}">
                                {{old('rates')}}
                            </textarea>
                            @error('rates')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3 px-3">
                        <label>Efectivo</label>
                        <div class="col-12 border rounded px-2">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <label for="simpleRoomCash">Habitación Simple en Efectivo</label>
                                    <input type="number" min="1" pattern="^[0-9]+" id="simpleRoomCash" placeholder="Habitación Simple" class="form-control @error('simple_room_cash') is-invalid @enderror"
                                           name="simple_room_cash" value="{{old('simple_room_cash')}}">
                                    @error('simple_room_cash')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-12 col-md-4">
                                    <label for="doubleRoomCash">Habitación Doble en Efectivo</label>
                                    <input type="number" min="1" pattern="^[0-9]+" id="doubleRoomCash" placeholder="Habitación Doble" class="form-control @error('doudle_room_cash') is-invalid @enderror"
                                           name="double_room_cash" value="{{old('doudle_room_cash')}}">
                                    @error('doudle_room_cash')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-12 col-md-4">
                                    <label for="tripleRoomCash">Habitación Triple en Efectivo</label>
                                    <input type="number" min="1" pattern="^[0-9]+" id="tripleRoomCash" placeholder="Habitación Triple" class="form-control @error('triple_room_cash') is-invalid @enderror"
                                           name="triple_room_cash" value="{{old('triple_room_cash')}}">
                                    @error('triple_room_cash')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 px-3">
                        <label>Tarjeta</label>
                        <div class="col-12 border rounded px-2">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <label for="simpleRoomCard">Habitación Simple en Tarjeta</label>
                                    <input type="number" min="1" pattern="^[0-9]+" id="simpleRoomCard" placeholder="Habitación Simple" class="form-control @error('simple_room_card') is-invalid @enderror"
                                           name="simple_room_card" value="{{old('simple_room_card')}}">
                                    @error('simple_room_card')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-12 col-md-4">
                                    <label for="doubleRoomCard">Habitación Doble en Tarjeta</label>
                                    <input type="number" min="1" pattern="^[0-9]+" id="doubleRoomCard" placeholder="Habitación Doble" class="form-control @error('double_room_card') is-invalid @enderror"
                                           name="double_room_card" value="{{old('double_room_card')}}">
                                    @error('double_room_card')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-12 col-md-4">
                                    <label for="tripleRoomCard">Habitación Triple en Tarjeta</label>
                                    <input type="number" min="1" pattern="^[0-9]+" id="tripleRoomCard" placeholder="Habitación Triple" class="form-control @error('triple_room_card') is-invalid @enderror"
                                           name="triple_room_card" value="{{old('triple_room_card')}}">
                                    @error('triple_room_card')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="px-3">
                            <div class="alert alert-info col-12" role="alert">
                                <i class="fa fa-file-image"></i>
                                Fotos (formato png, jpg o jpeg 1240 x 798 resolución 72 dpi, max 900Kb)
                            </div>
                        </div>
                        <div class="col-12">
                            <dropzone-component :reference="0" :type="'{{$type}}'"></dropzone-component>
                        </div>
                    </div>
                    @if($title != 'Ofertas')
                        <div class="row mt-3">
                            <div class="col-12 col-md-6">
                                <label for="">
                                    Latitud
                                </label>
                                <input type="text" placeholder="Latutide" class="form-control @error('latitude') is-invalid @enderror" name="latitude" id="latitude"  value="">
                                @error('latitude')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">
                                    Longitud
                                </label>
                                <input type="text" placeholder="Longitude" class="form-control @error('longitude') is-invalid @enderror" name="longitude" id="longitude" value="">
                                @error('longitude')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <label for="">
                                    Ubicación
                                </label>
                                <input type="text" class="form-control mt-3 @error('name') is-invalid @enderror"
                                       placeholder="Ubicación"
                                       required
                                       name="location" value="" id="location">
                                @error('location')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row px-3">
                            <div class="col-12 mt-5" id="map"></div>
                        </div>
                    @endif
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="leagues">Ligas</label>
                            <div class="border rounded p-3" id="leagues">
                                @foreach($leagues as $league)
                                    <div class="form-check">
                                        <input class="checkbox"
                                               value="{{$league->id}}"
                                               type="checkbox" name="leagues">
                                        <label class="form-check-label">
                                            {{ $league->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @if($title != 'Ofertas')
                        <div class="row mt-3">
                            <div class="col-12">
                                <label for="special_travels">Viajes Especiales</label>
                                <div class="border rounded p-3" id="special_travels">
                                    <div class="row">
                                        <div class="col-6 col-md-3">
                                            <input type="checkbox" class="checkbox"  name="pack_city" id="pack_city">
                                            <label for="pack_city">Paquete ciudad</label>
                                        </div>
                                        <div class="col-6 col-md-3">
                                            <input type="checkbox" class="checkbox"  name="museum_package" id="museum_package">
                                            <label for="museum_package">Entrada Museo</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Circuito de / a
                                            </label>
                                            <input type="text" placeholder="de...a" class="form-control @error('a_circuit') is-invalid @enderror"  name="a_cirtuit" id="a_cirtuit" value="{{old('a_circuit')}}">
                                            @error('a_circuit')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="circuit_days">Circuito dias</label>
                                            <input type="number" min="1" pattern="^[0-9]+" placeholder="dias" class="form-control @error('circuit_days') is-invalid @enderror"  name="circuit_days" id="circuit_days" value="{{old('circuit_days')}}">
                                            @error('circuit_days')
                                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Plan de Alimentos
                                            </label>
                                            <input type="text" placeholder="------" class="form-control @error('food_plan') is-invalid @enderror"  name="food_plan" id="food_plan" value="{{old('food_plan')}}">
                                            @error('food_plan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Idioma del plan
                                            </label>
                                            <input type="text" placeholder="idioma" class="form-control @error('lenguage_plan') is-invalid @enderror"  name="lenguage_plan" id="lenguage_plan" value="{{old('lenguage_plan')}}">
                                            @error('lenguage_plan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Crucero de / a
                                            </label>
                                            <input type="text" placeholder="de...a" class="form-control @error('cruise_from_to') is-invalid @enderror"  name="cruise_from_to" id="cruise_from_to" value="{{old('cruise_from_to')}}">
                                            @error('cruise_from_to')
                                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="go_suitable_transfer">Crucero dias</label>
                                            <input type="number" min="1" pattern="^[0-9]+" placeholder="dias" class="form-control @error('go_suitable_trnasfer') is-invalid @enderror"  name="go_suitable_transfer" id="go_suitable_transfer" value="{{old('go_suitable_trnasfer')}}">
                                            @error('go_suitable_trnasfer')
                                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Translado del Aeropuerto
                                            </label>
                                            <input type="text" placeholder="------" class="form-control @error('cruise_days') is-invalid @enderror"  name="cruise_days" id="cruise_days" value="{{old('cruise_days')}}">
                                            @error('cruise_days')
                                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Translado de / a
                                            </label>
                                            <input type="text" placeholder="de...a" class="form-control @error('go_transfer_from_to') is-invalid @enderror"  name="go_transfer_from_to" id="go_transfer_from_to" value="{{old('go_transfer_from_to')}}">
                                            @error('go_transfer_from_to')
                                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Boletos de Tren / clase
                                            </label>
                                            <input type="text" placeholder="clase" class="form-control @error('train_tickets') is-invalid @enderror"  name="train_tickets" id="train_tickets" value="{{old('train_tickets')}}">
                                            @error('train_tickets')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Boletos de Tren de / a
                                            </label>
                                            <input type="text" placeholder="de...a" class="form-control @error('train_tickets_from_to') is-invalid @enderror"  name="train_tickets_from_to" id="train_tickets_from_to" value="{{old('train_tickets_from_to')}}">
                                            @error('train_tickets_from_to')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <label for="">
                                                @if(App::islocale('es'))
                                                    <i class="fa fa-flag text-success"></i>
                                                @endif
                                                @if(App::islocale('en'))
                                                        <i class="fa fa-flag text-info"></i>
                                                    @endif
                                                    Ferry de / a
                                            </label>
                                            <input type="text" placeholder="de...a" class="form-control @error('ferry') is-invalid @enderror"  name="ferry" id="ferry" value="{{old('ferry')}}">
                                            @error('ferry')
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row my-3">
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Renta de coche incl. kilometraje libre y seguro básico de... a...
                                                    </label>
                                                    <input type="text" placeholder="de...a" class="form-control @error('car_rental') is-invalid @enderror"  name="car_rental" id="car_rental" value="{{old('car_rental')}}">
                                                    @error('car_rental')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Renta de coche / dias
                                                    </label>
                                                    <input type="number" min="1" pattern="^[0-9]+" placeholder="dias" class="form-control @error('rent_car_days') is-invalid @enderror"  name="rent_car_days" id="rent_car_days" value="{{old('rent_car_days')}}">
                                                    @error('rent_car_days')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row my-3">
                                                <div class="col-12 col-md-3">
                                                    <label for="walk">Paseo por la ciudad / Horas</label>
                                                    <input type="number" min="1" pattern="^[0-9]+" placeholder="horas" class="form-control @error('walk') is-invalid @enderror"  name="walk" id="walk" value="{{old('walk')}}">
                                                    @error('walk')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="foods">Comidas dias</label>
                                                    <input type="number" min="1" pattern="^[0-9]+" placeholder="dias" class="form-control @error('foods') is-invalid @enderror"  name="foods" id="foods" value="{{old('foods')}}">
                                                    @error('foods')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="dinners">Cenas dias</label>
                                                    <input type="number" min="1" pattern="^[0-9]+" placeholder="dias" class="form-control @error('dinners') is-invalid @enderror"  name="dinners" id="dinners" value="{{old('dinners')}}">
                                                    @error('dinners')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row my-3">
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Excursión 1
                                                    </label>
                                                    <input type="text" placeholder="idioma" class="form-control @error('excursion_1') is-invalid @enderror"  name="excursion_1" id="excursion_1" value="{{old('excursion_1')}}">
                                                    @error('excursion_1')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Excursión 2
                                                    </label>
                                                    <input type="text" placeholder="idioma" class="form-control @error('excursion_2') is-invalid @enderror"  name="excursion_2" id="excursion_2" value="{{old('excursion_2')}}">
                                                    @error('excursion_2')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Excursión 3
                                                    </label>
                                                    <input type="text" placeholder="idioma" class="form-control @error('excursion_3') is-invalid @enderror"  name="excursion_3" id="excursion_3" value="{{old('excursion_3')}}">
                                                    @error('excursion_3')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row my-3">
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Entrada a Espectaculo
                                                    </label>
                                                    <input type="text" placeholder="----" class="form-control @error('entry') is-invalid @enderror"  name="entry" id="entry" value="{{old('entry')}}">
                                                    @error('entry')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Pases
                                                    </label>
                                                    <input type="text" placeholder="----" class="form-control @error('passes') is-invalid @enderror"  name="passes" id="passes" value="{{old('passes')}}">
                                                    @error('passes')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row my-3">
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Otro 1
                                                    </label>
                                                    <input type="text" placeholder="-----" class="form-control @error('see_another_1') is-invalid @enderror"  name="see_another_1" id="see_another_1" value="{{old('see_another_1')}}">
                                                    @error('see_another_1')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Otro 2
                                                    </label>
                                                    <input type="text" placeholder="-----" class="form-control @error('see_another_2') is-invalid @enderror"  name="see_another_2" id="see_another_2" value="{{old('see_another_2')}}">
                                                    @error('see_another_2')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <label for="">
                                                        @if(App::islocale('es'))
                                                            <i class="fa fa-flag text-success"></i>
                                                        @endif
                                                        @if(App::islocale('en'))
                                                            <i class="fa fa-flag text-info"></i>
                                                        @endif
                                                        Otro 3
                                                    </label>
                                                    <input type="text" placeholder="-----" class="form-control @error('see_another_3') is-invalid @enderror"  name="see_another_3" id="see_another_3" value="{{old('see_another_3')}}">
                                                    @error('see_another_3')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    @endif
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="special_travels">General</label>
                            <div class="border rounded p-3" id="general">
                                <div class="row">
                                    <div class="col-12 col-md-3">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <i class="fa fa-flag text-success"></i>
                                            @endif
                                            @if(App::islocale('en'))
                                                <i class="fa fa-flag text-info"></i>
                                            @endif
                                            Guía acompañante / de - a
                                        </label>
                                        <input type="text" placeholder="de...a" class="form-control @error('guide') is-invalid @enderror"  name="guide" id="guide" value="{{old('guide')}}">
                                        @error('guide')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <i class="fa fa-flag text-success"></i>
                                            @endif
                                            @if(App::islocale('en'))
                                                <i class="fa fa-flag text-info"></i>
                                            @endif
                                            Guía local de idioma
                                        </label>
                                        <input type="text" placeholder="-----" class="form-control @error('local_guide') is-invalid @enderror"  name="local_guide" id="local_guide" value="{{old('local_guide')}}">
                                        @error('local_guide')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <i class="fa fa-flag text-success"></i>
                                            @endif
                                            @if(App::islocale('en'))
                                                <i class="fa fa-flag text-info"></i>
                                            @endif
                                            Seguro de Cancelación
                                        </label>
                                        <input type="text" placeholder="-----" class="form-control @error('sure') is-invalid @enderror"  name="sure" id="sure" value="{{old('sure')}}">
                                        @error('sure')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <i class="fa fa-flag text-success"></i>
                                            @endif
                                            @if(App::islocale('en'))
                                                <i class="fa fa-flag text-info"></i>
                                            @endif
                                            Seguro de enfermedad y accidentes
                                        </label>
                                        <input type="text" placeholder="-----" class="form-control @error('health_insurance') is-invalid @enderror"  name="health_insurance" id="health_insurance" value="{{old('health_insurance')}}">
                                        @error('health_insurance')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <i class="fa fa-flag text-success"></i>
                                            @endif
                                            @if(App::islocale('en'))
                                                <i class="fa fa-flag text-info"></i>
                                            @endif
                                            Visado para
                                        </label>
                                        <input type="text" placeholder="-----" class="form-control @error('visa_for') is-invalid @enderror"  name="visa_for" id="visa_for" value="{{old('visa_for')}}">
                                        @error('visa_for')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-12 col-md-7">
                                        <label for="assistance">
                                        <input type="checkbox" class="checkbox"  name="assistance" id="assistance">
                                        Asistencia en el destino
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <label>
                                            <input type="checkbox" class="checkbox"  name="trunks" id="trunks">
                                        Servicio de maleteros en aeropuertos
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <label for="carger">
                                        <input type="checkbox" class="checkbox"  name="carger" id="carger">
                                        Cargos or Servicios
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <label for="taxes">
                                        <input type="checkbox" class="checkbox"  name="taxes" id="taxes">
                                        Todos los impuestos
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <label for="emergency">
                                        <input type="checkbox" class="checkbox"  name="emergency" id="emergency">
                                        Contacto 24/7 para casos de emergencia
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <label for="local_contact">
                                        <input type="checkbox" class="checkbox"  name="local_contact" id="local_contact">
                                        Contacto Local
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <label for="label">
                                        <input type="checkbox" class="checkbox"  name="labels" id="labels">
                                        Porta etiquetas
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <label for="folders">
                                        <input type="checkbox" class="checkbox"  name="folders" id="folders">
                                        Carpeta de documentos
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <label for="destination">
                                        <input type="checkbox" class="checkbox"  name="destination" id="destination">
                                        Informacion del destino
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <label for="itinerary">
                                        <input type="checkbox" class="checkbox"  name="itinerary" id="itinerary">
                                        Itinerario
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <label for="exhibitor_tips">
                                        <input type="checkbox" class="checkbox"  name="exhibitor_tips" id="exhibitor_tips">
                                        Consejos para expositores de ferias
                                        </label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <label for="tips">
                                        <input type="checkbox" class="checkbox"  name="tips" id="tips">
                                        Consejos para visitantes de ferias
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <input type="checkbox" class="checkbox" name="active" id="active" checked>
                            <label for="active">Activo</label>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-edit"></i>
                                Crear
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input type="hidden" id="language" value="{{ config('app.locale') }}">
@endsection
@section('scripts')
    <script>
        $(window).ready(function (){
            var language = $("#language").val();
            var title =  @json($title);
            var destinationsAll = @json($destinationsAll);
            var destinationsMexico = @json($destinationsMexico);
            var destinations = $('#destination_id');
            $('#region').change(function (){
                destinations.empty();
                region_id = this.value;
                if (region_id == 14){
                    destinationsMexico.forEach(function(element) {
                        destinations.append($("<option>", {
                            value: element.id,
                            text:  language == 'es' ? element.region.es:element.region.en
                        }));
                    });
                } else {
                    destinationsAll.forEach(function(element) {
                        if (element.region_id == region_id){
                            destinations.append($("<option>", {
                                value: element.id,
                                text:  language == 'es' ? element.destination.es:element.destination.en
                            }));
                        }
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        var map;
        var geocoder;
        // window.addEventListener("load", loadingPage);
        // function loadingPage(){
        //     var text_latitude = document.getElementById("latitude");
        //     text_latitude.value = latitude;
        //     var text_longitude = document.getElementById("longitude");
        //     text_longitude.value = longitude;
        // }


        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('poster');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        function updateMarkerPosition(latLng) {
            var text_latitude = document.getElementById("latitude");
            var text_longitude = document.getElementById("longitude");
            text_latitude.value = latLng.lat();
            text_longitude.value = latLng.lng();
        }

        function updateMarkerAddress(str) {
            //document.getElementById('address').innerHTML = str;
            var text_address = document.getElementById('location');
            text_address.value = str;
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function(responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function initMap() {
            var mapLayer = document.getElementById("map");
            var centerCoordinates = new google.maps.LatLng('19.432635251663157', '-99.13232105331417');
            var defaultOptions = { center: centerCoordinates, zoom: 8 }

            map = new google.maps.Map(mapLayer, defaultOptions);
            geocoder = new google.maps.Geocoder();

            geocoder.geocode( { 'address': 'Centro Histórico de la Cdad. de México' }, function(LocationResult, status) {
                // if (status == google.maps.GeocoderStatus.OK) {
                //     var latitude = LocationResult[0].geometry.location.lat();
                //     var longitude = LocationResult[0].geometry.location.lng();
                // }
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(19.432635251663157, -99.13232105331417),
                    map: map,
                    title: 'KomexTours',
                    draggable: true,
                    clickable: true,
                    animation: google.maps.Animation.DROP
                });
                // Update current position info.
                updateMarkerPosition(marker.getPosition());
                geocodePosition(marker.getPosition());

                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function() {
                    updateMarkerAddress('Dragging...');
                });

                google.maps.event.addListener(marker, 'drag', function() {
                    updateMarkerPosition(marker.getPosition());
                });

                google.maps.event.addListener(marker, 'dragend', function() {
                    geocodePosition(marker.getPosition());
                });
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8_8kSrBymZR_OLHLeCfWZFrX7l1HNUTE&callback=initMap"></script>
@endsection

