@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Editar Promoción</h1>
                        <div class="col-2">
                            <a href="{{route('promotions.index')}}" class="btn btn-primary">
                                <i class="fa fa-list"></i>
                                Lista
                            </a>
                        </div>
                    </div>
                </div>
                <form class="validate-form mb-5" method="POST" action="{{route('promotions.update', ['promotion' => $promotion->id])}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Nombre
                            </label>
                            <input type="text" class="form-control mt-3 @error('name') is-invalid @enderror" placeholder="Nombre" name="name" value="{{old('name', $promotion->name)}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <p class="auto-style3">
                                <input checked="checked" name="opcion" class="pago" type="radio"  value="descripción" onclick="optionFunction(this)"/>
                                <span> Descripción</span>
                                <input class="pago" name="opcion" type="radio" value="url" onclick="optionFunction(this)"/>
                                <span> Url</span>
                            </p>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12" style="display:;" id="content">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Contenido
                            </label>
                            <textarea class="ckeditor mt-3 @error('content') is-invalid @enderror" placeholder="Contenido"
                                      name="intro" value="{{old('content', $promotion->content)}}">{{old('content', $promotion->content)}}</textarea>
                            @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-12" style="display:none;" id="url">
                            <label for="">Liga</label>
                            <input type="text" class="form-control mt-3 @error('url') is-invalid @enderror" placeholder="Url" name="url" value="{{old('url', $promotion->url)}}">
                            @error('url')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Imagen</label>
                            <br>
                            @if($promotion->image)
                                <img src="{{asset($promotion->image)}}" class="img-fluid image" alt="{{$promotion->image}}" id="image">
                            @else
                                <img src="{{asset('images/missing.png')}}" class="img-fluid image" alt="Sin imagen" id="image">
                            @endif
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Imagen (Formato png, jpg o jpeg 1240 x 798 resolución 72 dpi max 900kb) <span class="text-danger">¡Importante! Todas las imagenes deben del mismo tamaño</span><br>
                            </div>

                            <div class="upload-btn-wrapper" id="btn">
                                <button class="btn btn-primary">Selecciona un archivo</button>
                                <input type="file" name="image" accept="image/*" onchange="loadFile(event)">
                            </div>
                            <div id="delete">
                                <a class="btn btn-danger" onclick="deletePoster()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i>
                                Actualizar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        document.getElementById('delete').style.display = 'none';

        function optionFunction(option){
            if (option.value == 'url'){
                document.getElementById('content').style.display = 'none';
                document.getElementById('url').style.display = 'block';
            } else {
                document.getElementById('content').style.display = 'block';
                document.getElementById('url').style.display = 'none';
            }
        }

        function deletePoster(){
            document.getElementById('delete').style.display = 'none';
            document.getElementById('btn').style.display = 'block';
            var image = document.getElementById('image');
            image.src = '/images/missing.png';
            image.value = "";
        }
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('image');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btn').style.display = 'none';
            document.getElementById('delete').style.display = 'block';
        };
    </script>
@endsection
