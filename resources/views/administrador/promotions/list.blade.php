@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{route('promotions.create')}}" class="-btn-agregar">
                    <i class="fas fa-user-plus"></i>
                    Agregar
                </a>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Promociones
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover mt-3">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Liga</th>
                                <th colspan="2">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($promotions->sortby('name') as $promotion)
                                <tr>
                                    <td>{{$promotion->id}}</td>
                                    <td width="200px">
                                        <a href="{{route('promotions.edit', ['promotion' => $promotion->id])}}">
                                            @if($promotion->image)
                                                <img src="{{asset($promotion->image)}}" class="img-fluid" alt="{{$promotion->image}}" id="image">
                                            @else
                                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="image">
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        @if($promotion->name)
                                            <a href="{{route('promotions.edit', ['promotion' => $promotion->id])}}">{{$promotion->name}}</a>
                                        @else
                                            <a href="{{route('promotions.edit', ['promotion' => $promotion->id])}}">Editar</a>
                                        @endif
                                    </td>
                                    <td>{{$promotion->url}}</td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('promotions.active', ['id' => $promotion->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($promotion->active)
                                                    <i class="fa fa-power-off -on"></i>
                                                @else
                                                    <i class="fa fa-power-off -off"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('promotions.destroy', ['promotion' => $promotion->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
