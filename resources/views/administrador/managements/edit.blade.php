@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Actualizar Destination Management</h1>
                        <div class="col-2">
                            <a href="{{route('management.index')}}" class="btn btn-primary">
                                <i class="fa fa-list"></i>
                                Lista
                            </a>
                        </div>
                    </div>
                </div>
                <form class="validate-form mb-5" method="POST" action="{{route('management.update', [$management->id])}}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <label for="">Liga</label>
                            <input type="text" class="form-control mt-3 @error('url') is-invalid @enderror" placeholder="Url" required name="url" value="{{old('url', $management->url)}}">
                            @error('url')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Imagen</label>
                            <br>
                            @if($management->image)
                                <img src="{{asset($management->image)}}" class="img-fluid" alt="{{$management->image}}" id="poster">
                            @else
                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="poster">
                            @endif
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Imagen (Formato png, jpg o jpeg 224 x 170 resolución 72 dpi max 900kb)<br>
                            </div>

                            <div class="upload-btn-wrapper" id="btnposter">
                                <button class="btn btn-primary">Selecciona un archivo</button>
                                <input type="file" name="image" accept="image/*" onchange="loadFilePoster(event)">
                            </div>
                            <div id="deleteposter">
                                <a class="btn btn-danger" onclick="deletePoster()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        document.getElementById('deleteposter').style.display = 'none';
        function deletePoster(){
            document.getElementById('deleteposter').style.display = 'none';
            var image = document.getElementById('poster');
            image.src = '/images/missing.png';
            image.value = "";
        }
        var loadFilePoster = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('poster');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btnposter').style.display = 'none';
            document.getElementById('deleteposter').style.display = 'block';
        };
    </script>
@endsection

